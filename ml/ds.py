import os
import json
import fnmatch
import datetime

import numpy
import numpy as np
from PIL import Image, ImageDraw
import torch
from torch.utils.data import Dataset, DataLoader
from torchvision.datasets import CocoDetection
from torchvision import transforms as T


class AbstractDataset(Dataset):

    def __init__(self, name, img_dir, transforms=None):
        self.transforms = transforms
        self.name = name
        self.img_dir = img_dir

    def save_classes(self, classes):
        """Saves the classes2labels into a class file on the datasets images folder as JSON."""
        class_fn = os.path.join(self.img_dir, f'{len(classes)}classes_raptor.json')
        with open(class_fn, 'w') as fp:
            json.dump(classes, fp)

    def save_mask_images(self, idx, masks, masks_dir):
        """Saves all masks of the image with index idx of the dataset. Masks are stored into the same folder as the
        images and files are named '<name>-<idx>-<no of mask>.png'. Input masks are tensors."""
        for mask in masks:
            # convert binary mask to grayscale image
            img = T.ToPILImage()(mask*255).convert("L")
            d = f"{datetime.datetime.now():%Y%m%d-%H%M%S}"
            img.save(os.path.join(masks_dir, f'{self.name}-{idx}-{d}.png'))

    def save_image(self, idx, image, boxes, classes, image_dir):
        """Saves the image with index idx of the dataset. Image files are named '<dataset name>-<idx>.png'.
        Input images are tensors."""

        img = T.ToPILImage()(image).convert("RGB")

        i = ImageDraw.Draw(img)
        for box, class_ in zip(boxes, classes):
            shape = (box[0], box[1], box[2], box[3])
            i.rectangle(xy=shape, outline="#ff000022", width=1)
            i.text(xy=(box[0], box[1] - 10), text=class_, fill="#ff000022", align="left")

        d = f"{datetime.datetime.now():%Y%m%d-%H%M%S}"
        img.save(os.path.join(image_dir, f'{self.name}-{idx}-{d}.png'))


class TestDataset(AbstractDataset):
    """
    Represents a torch vision dataset of a folder with test images.
    This class can be used for test/evaluation/inference and provides the test images as an iterable dataset.
    """

    def __init__(self, name, img_dir, transforms=None, pattern='*'):
        super().__init__(name, img_dir, transforms)
        self.image_files = [name for name in os.listdir(self.img_dir)
                            if os.path.isfile(os.path.join(self.img_dir, name))
                            and fnmatch.fnmatch(name, pattern)]

    def __len__(self):
        """Returns size of the dataset."""
        return len(self.image_files)

    def __getitem__(self, idx):
        """Returns image with index idx from dataset."""
        image_file = os.path.join(self.img_dir, self.image_files[idx])
        img = np.array(Image.open(image_file).convert("RGB"))
        # if we got transformers, then apply them now
        if self.transforms is not None:
            transformed = self.transforms(
                image=img,
            )
            img = transformed["image"]
        return img


class RaptorMaps12ClassDataset(AbstractDataset):
    """
    Represents a torch vision dataset of the RaptorMaps photo voltaic modules.
    This class uses all available classes from the original dataset and of course background).

    The class creates bounding boxes and masks on the fly for each image,
    the class labels are taken from the RaptorMaps meta data JSON file.

    When transforms are given, those transforms are expecting an image.
    """

    annotations_file = "module_metadata.json"

    def __init__(self, name, img_dir, transforms=None, save_images=False, save_masks=False):
        """
        Initializes the dataset.
        :param name: Name of the dataset. Used for e.g. saving generated files.
        :param img_dir: Directory name where images are stored.
        :param transforms: Torch transformations which will be applied to images and masks.
        :param save_images: If images should be saved or not.
        :param save_masks: If masks should be saved as images or not.
        """
        super().__init__(name, img_dir, transforms=transforms)
        self.save_images = save_images
        self.save_masks = save_masks
        self.classes2labels, self.labels2classes, self.img_labels = \
            self.read_image_labels(os.path.join(self.img_dir, self.annotations_file))
        self.save_classes(self.classes2labels)
        self.num_classes = len(self.classes2labels)

        d = f"{datetime.datetime.now():%Y%m%d-%H%M%S}"

        if self.save_images:
            self.image_dir = os.path.join(self.img_dir, 'gen-images', d)
            if not os.path.exists(self.image_dir) or not os.path.isdir(self.image_dir):
                os.makedirs(self.image_dir)
            print(f"Saving augmented images to folder: {self.image_dir}")

        if self.save_masks:
            self.masks_dir = os.path.join(self.img_dir, 'masks', d)
            if not os.path.exists(self.masks_dir) or not os.path.isdir(self.masks_dir):
                os.makedirs(self.masks_dir)
            print(f"Saving masks to folder: {self.masks_dir}")

    def read_image_labels(self, annotations_file):
        """Reads image class labels from annotation file."""

        # contains the unique class names, generated from the label annotation file
        classes2labels = {"background": 0}
        labels2classes = {"0": "background"}

        with open(annotations_file) as f:
            image_labels = json.load(f)
            # extract class labels and assign them a unique id
            i = 1
            for key, value in image_labels.items():
                if not value['anomaly_class'] in classes2labels:
                    classes2labels[value['anomaly_class']] = i
                    labels2classes[i] = value['anomaly_class']
                    i += 1

            return classes2labels, labels2classes, image_labels

    def __len__(self):
        """Returns size of the dataset."""
        return len(self.img_labels)

    def __getitem__(self, idx):
        """Returns image with index idx from dataset."""

        def bbox_for_mask(img):
            rows = np.any(img, axis=1)
            cols = np.any(img, axis=0)
            rmin, rmax = np.where(rows)[0][[0, -1]]
            cmin, cmax = np.where(cols)[0][[0, -1]]
            return cmin, rmin, cmax + 1, rmax + 1

        image_file = os.path.join(self.img_dir, self.img_labels[f'{idx}']['image_filepath'])
        img = np.array(Image.open(image_file))

        # get numerical class 'label' from  text label
        label = self.classes2labels[self.img_labels[f'{idx}']['anomaly_class']]
        labels = [label]

        # the RaptorMaps PV module images have a rectangular shape of 24x40
        boxes = [[0, 0, img.shape[1]-1, img.shape[0]-1]]

        # create binary masks. (0 means background pixel, 1 means object pixel)
        mask = np.ones((img.shape[1], img.shape[0]))
        masks = [np.array(mask)]

        # if we got transformers, then apply them now
        if self.transforms is not None:
            class_labels = None
            while class_labels is None or len(class_labels) == 0:
                try:
                    transformed = self.transforms(
                        image=img,
                        masks=masks,
                        bboxes=boxes,
                        labels=labels
                    )
                    img = transformed["image"]
                    masks = transformed["masks"]
                    boxes = transformed["bboxes"]
                    labels = transformed["labels"]
                    class_labels = labels
                    masks = [i for i in masks if i.any()]  # filter masks which vanished during transforms
                    boxes = [bbox_for_mask(mask.detach().numpy()) for mask in masks]
                except AssertionError as e:
                    print(f'Error at image with idx {idx}: {e}')

        try:
            boxes = np.asarray(boxes)
            area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        except IndexError:
            print(f'IndexError at image with idx={idx}: {image_file}')
            print(transformed)
            area = 1

        # convert everything into a torch.Tensor
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.as_tensor(labels, dtype=torch.int64)
        masks = torch.stack(masks)
        area = torch.as_tensor(area, dtype=torch.float32)
        image_id = torch.tensor([idx])

        num_objs = 1  # we have only one object instance on our images
        # suppose all instances are not crowd
        iscrowd = torch.zeros((num_objs,), dtype=torch.int64)

        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["masks"] = masks
        target["image_id"] = image_id
        target["area"] = area
        target["iscrowd"] = iscrowd

        # if we need to save the generated masks, do so now
        if self.save_masks and self.masks_dir is not None:
            self.save_mask_images(idx, target["masks"], self.masks_dir)

        # if we need to save the images, do so now
        if self.save_images and self.image_dir is not None:
            classes = [self.labels2classes[label.item()] for label in labels]
            self.save_image(idx, img, boxes, classes, self.image_dir)

        return img, target


class RaptorMaps2ClassDataset(RaptorMaps12ClassDataset):
    """
    Represents a torch vision dataset of the RaptorMaps photo voltaic modules.
    This class will only contain two classes, No-Anomaly and Anomaly (and of course background).
    For this the read_image_labels converts any anomaly class to 'Anomaly'.

    The class creates bounding boxes and masks on the fly for each image,
    the class label are taken from a meta data JSON file.
    """

    def read_image_labels(self, annotations_file):
        """Reads image class labels from annotation file. Converts all non-'No-Anomaly' classes2labels to 'Anomaly' class."""

        # contains the unique class names, generated from the label annotation file
        classes2labels = {"background": 0}
        labels2classes = {"0": "background"}

        with open(annotations_file) as f:
            import json
            image_labels = json.load(f)

            # extract class labels and assign them a unique id
            i = 1
            for key, value in image_labels.items():

                # cheat with class labels. we reduce to two class labels: Anomaly and No-Anomaly
                if value['anomaly_class'] != "No-Anomaly":
                    value['anomaly_class'] = "Anomaly"

                if not value['anomaly_class'] in classes2labels:
                    classes2labels[value['anomaly_class']] = i
                    labels2classes[i] = value['anomaly_class']
                    i += 1

        return classes2labels, labels2classes, image_labels


class CocoLikeDataset(CocoDetection):
    """Implementation for a COCO like annotated dataset for use with a DataLoader implementation."""

    def __init__(self, name, root=None, annFile=None, transforms=None,
                 save_images=False, save_masks=False):
        """Initialized a COCO based dataset."""

        self.align_images_and_annotations(img_dir=root, annFile=annFile)
        super().__init__(root=root, annFile=f"{annFile}.tmp")
        self.name = name
        self.img_dir = root
        self.save_images = save_images
        self.save_masks = save_masks
        d = f'{datetime.datetime.now():%Y%m%d-%H%M%S}'
        if self.save_images:
            self.image_dir = os.path.join(self.img_dir, 'gen-images', d)
            if not os.path.exists(self.image_dir) or not os.path.isdir(self.image_dir):
                os.makedirs(self.image_dir)
            print(f"Saving augmented images to folder: {self.image_dir}")

        if self.save_masks:
            self.masks_dir = os.path.join(self.img_dir, 'gen-masks', d)
            if not os.path.exists(self.masks_dir) or not os.path.isdir(self.masks_dir):
                os.makedirs(self.masks_dir)
            print(f"Saving masks to folder: {self.masks_dir}")

        # avoid using self.transforms, as it is already a class attribute from CocoDetection
        self.transforms_ = transforms

        print(f"Files in dataset: {len(self.ids)}, annotations: {len(self.coco.anns)}")

    def __getitem__(self, idx):
        """Returns the item with index idx from the dataset.
           The item contains a dictionary with the elements
           "image_id", "labels", "boxes", "area", "masks", "iscrowd"."""

        image, targets = super().__getitem__(idx)

        image = np.asarray(image)
        image_id = self.ids[idx]

        # convert torchvision style targets to albumentations style targets
        boxes, labels, masks, areas = self.convert_target_style(image_id, targets)

        # if transformers given, apply them now
        if self.transforms_ is not None:
            image, labels, boxes, masks, areas = \
                self.apply_transforms(image_id, image, labels, boxes, masks)

        # if we need to save the generated masks, do so now
        if self.save_masks and self.masks_dir is not None:
            self.save_mask_images(image_id, masks, self.masks_dir)

        # if we need to save the images, do so now
        if self.save_images and self.image_dir is not None:
            self.save_image(image_id, image,
                            boxes, labels, self.image_dir)

        # convert everything to a torch.Tensor
        image = torch.as_tensor(image)
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.as_tensor(labels, dtype=torch.int64)
        if len(masks) > 0 and type(masks[0]) == np.ndarray:
            masks = [torch.from_numpy(mask) for mask in masks]
        masks = torch.stack(masks)
        areas = torch.as_tensor(areas, dtype=torch.float32)
        image_id = torch.tensor(image_id)
        # suppose all instances are not crowd
        iscrowd = torch.zeros((len(labels),), dtype=torch.int64)

        # return image and targets
        targets = {
            "image_id": image_id,
            "labels":   labels,
            "boxes":    boxes,
            "masks":    masks,
            "area":     areas,
            "iscrowd":  iscrowd
        }

        return image, targets

    def convert_target_style(self, image_id, targets):
        """Converts the COCO style targets to targets needed by Mask R-CNN."""
        boxes = []
        labels = []
        masks = []
        iscrowd = []
        areas = []
        for target in targets:
            # convert bbox from COCO format to Pascal VOC format
            pascal_voc_box = (target['bbox'][0], target['bbox'][1],
                              target['bbox'][0] + target['bbox'][2], target['bbox'][1] + target['bbox'][3])
            boxes.append(pascal_voc_box)
            labels.append(target['category_id'])
            iscrowd.append(target['iscrowd'])
            areas.append(target['area'])
            try:
                masks.append(self.coco.annToMask(target))
            except Exception as e:
                print(f"Exception {e}: image_id: {image_id}")
        return boxes, labels, masks, areas

    def apply_transforms(self, image_id, image, labels, boxes, masks):
        """Transforms the COCO annotation targets to format needed for Mask R-CNN."""
        def bbox_for_mask(img):
            rows = np.any(img, axis=1)
            cols = np.any(img, axis=0)
            rmin, rmax = np.where(rows)[0][[0, -1]]
            cmin, cmax = np.where(cols)[0][[0, -1]]
            return cmin, rmin, cmax + 1, rmax + 1

        transform_ok = False
        while not transform_ok:
            try:
                transformed = self.transforms_(
                    image=image,
                    masks=masks,
                    bboxes=boxes,
                    labels=labels
                )
                image_ = transformed["image"]
                masks_ = transformed["masks"]
                boxes_ = transformed["bboxes"]
                labels_ = transformed["labels"]
                masks_ = [i for i in masks_ if i.any()]  # filter masks which vanished during transforms
                boxes_ = [bbox_for_mask(mask.detach().numpy()) for mask in masks_]
                transform_ok = len(boxes_) == len(labels_) == len(masks_)
                if not transform_ok:
                    print(f'Problem with image {image_id} after transforms: '
                          f'#boxes: {len(boxes_)} != #masks: {len(masks_)} != #labels: {len(labels_)}')
            except AssertionError as e:
                print(f'Error at image with image_id {image_id}: {e}')

        # take results only when transformation valid
        if len(boxes_) == len(labels_) == len(masks_) and len(boxes_) > 0:
            image, masks, boxes, labels = image_, masks_, boxes_, labels_

        # calculate areas of targets for small/medium/large classification.
        try:
            boxes = np.asarray(boxes)
            areas = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        except IndexError:
            print(f'IndexError at image with image_id {image_id}')
            areas = 1

        return image, labels, boxes, masks, areas

    def save_mask_images(self, image_id, masks, masks_dir):
        """Saves all masks of the image of the dataset.
           Masks are stored into the same folder as the images and files are named
           '<name>-<image_id>-<no of mask>.png'. Input masks are tensors."""

        for mask in masks:
            # convert binary mask to grayscale image
            img = T.ToPILImage()(mask*255).convert("L")
            d = f'{datetime.datetime.now():%Y%m%d-%H%M%S}'
            img.save(os.path.join(masks_dir, f'{self.name}-{image_id}-{d}.png'))

    def save_image(self, image_id, image, boxes, classes, image_dir):
        """Saves the image of the dataset.
           Image files are named '<dataset name>-<image_id>.png'.
           Input images are tensors."""

        img = T.ToPILImage()(image).convert("RGB")

        i = ImageDraw.Draw(img)
        for box, class_ in zip(boxes, classes):
            shape = (box[0], box[1], box[2], box[3])
            i.rectangle(xy=shape, outline="#ff000022", width=1)
            i.text(xy=(box[0], box[1] - 10), text=f'{class_}', fill="#ff000022", align="left")

        d = f'{datetime.datetime.now():%Y%m%d-%H%M%S}'
        img.save(os.path.join(image_dir, f'{self.name}-{image_id}-{d}.png'))

    def align_images_and_annotations(self, img_dir, annFile):
        """Synchronizes images and annotations.
           Remove images without annotations and vice versa."""

        with open(annFile, 'r') as f:
            d = json.load(f)

        images = []
        annotations = []
        img_count = 0
        anns_count = 0
        removed = 0
        for i in d['images']:
            anns = []
            for a in d['annotations']:
                if a['image_id'] == i['id']:
                    anns.append(a)

            n = os.path.join(img_dir, i['file_name'])
            file_exists = os.path.exists(n) and os.path.isfile(n)
            if anns is not None and len(anns) > 0 and file_exists:
                print(f"Processing file {i['file_name']}")
                images.append(i)
                annotations += anns
                img_count += 1
                anns_count += len(anns)
            else:
                reason = "not exist!" if not file_exists else "no annotations!"
                print(f'Removed file {i["file_name"]} from dataset, {reason}')
                removed += 1

        d['images'] = images
        d['annotations'] = annotations

        with open(f"{annFile}.tmp", 'w') as f:
            json.dump(d, f, indent=2)

        print(f'Totally removed {removed} files from dataset!')
        print(f'Images in dataset: {img_count}, annotations {anns_count}.')


class OnlineMeanStd:
    """
    Copied over and modified for my needs:
    https://github.com/Nikronic/CoarseNet/blob/master/utils/preprocess.py#L142-L200
    """
    def __init__(self):
        pass

    def __call__(self, dataset):
        """
        Calculate mean and std of a dataset in lazy mode (online)

        :param dataset: Dataset object corresponding to your dataset
        :param batch_size: higher size, more accurate approximation
        :return: A tuple of (mean, std) with size of (3,)
        """

        loader = DataLoader(dataset=dataset, batch_size=1, shuffle=False,
                            num_workers=1, pin_memory=False)
        cnt = 0
        fst_moment = torch.empty(3)
        snd_moment = torch.empty(3)

        for data in loader:
            b, c, h, w = data.shape
            nb_pixels = b * h * w
            sum_ = torch.sum(data, dim=[0, 2, 3])
            sum_of_square = torch.sum(data ** 2, dim=[0, 2, 3])
            fst_moment = (cnt * fst_moment + sum_) / (cnt + nb_pixels)
            snd_moment = (cnt * snd_moment + sum_of_square) / (cnt + nb_pixels)
            if any(numpy.isnan(snd_moment)):
                print("NaN!")

            cnt += nb_pixels

        return fst_moment, torch.sqrt(snd_moment - fst_moment ** 2)
