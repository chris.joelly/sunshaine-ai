import os.path
import json
import datetime
import numpy as np

import torch
from torch.utils.data import DataLoader
from torchvision.models.detection import mask_rcnn
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.mask_rcnn import MaskRCNNPredictor
from torchvision.models.detection.anchor_utils import AnchorGenerator

import utils
from engine import train_one_epoch, evaluate


class ModelTrainer:
    """Class manages a custom Mask R-CNN model adapted for a num_classes class dataset."""

    def __init__(self, model=None, params=None, **kwargs):
        """Create a model trainer for the given model and parameters.
           If no model given, a new model is created."""

        assert params is not None
        self.params = params
        if model is not None:
            self.model = model
        else:
            self.model = self.__create_model__(self.params['num_classes'], **kwargs)
        self.model_name = None
        self.stats = dict()
        self.test_stats = dict()
        self.losses = None

    def __create_model__(self, num_classes, **kwargs):
        """Creates a Mask R-CNN model for num_classes classes."""

        # include background in classes
        self.num_classes = num_classes + 1

        # as our objects are 40x24, we adapt the anchors
        anchor_sizes = ((8,), (16,), (32,), (64,), (128,), )
        aspect_ratios = ((0.5, 1.0, 2.0),) * len(anchor_sizes)
        anchor_generator = AnchorGenerator(sizes=anchor_sizes, aspect_ratios=aspect_ratios)

        # load an instance segmentation model
        self.model = mask_rcnn.maskrcnn_resnet50_fpn(weights=None,
                                                     weights_backbone=None,
                                                     image_mean=[0, 0, 0], image_std=[1, 1, 1],
                                                     rpn_anchor_generator=anchor_generator,
                                                     **kwargs)

        # get number of input features for the classifier
        in_features = self.model.roi_heads.box_predictor.cls_score.in_features
        # replace head with a new one which is aligned to the number of classes
        self.model.roi_heads.box_predictor = FastRCNNPredictor(in_features, self.num_classes)

        # get the number of input features for the mask classifier
        in_features_mask = self.model.roi_heads.mask_predictor.conv5_mask.in_channels
        hidden_layer = 256
        # and replace the mask predictor with a new one
        self.model.roi_heads.mask_predictor = MaskRCNNPredictor(in_features_mask,
                                                                hidden_layer,
                                                                self.num_classes)

        return self.model

    def train_model(self, dataset, test_dataset, train_val_split=0.8):
        """Trains the model on dataset using the given configuration."""

        # train on the GPU or on the CPU, if a GPU is not available
        device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

        # split the dataset in train and test set (e.g. train_val_split=0.8)
        train_size = int(len(dataset) * train_val_split)
        val_size = len(dataset) - train_size
        train_set, val_set = torch.utils.data.random_split(dataset, [train_size, val_size])

        # define training, validation and test data loaders
        data_loader_train = DataLoader(
            train_set, batch_size=self.params['batch_size'], shuffle=True,
            collate_fn=utils.collate_fn)

        data_loader_val = DataLoader(
            val_set, batch_size=self.params['batch_size'], shuffle=False,
            collate_fn=utils.collate_fn)

        data_loader_test = DataLoader(
            test_dataset, batch_size=self.params['batch_size'], shuffle=False,
            collate_fn=utils.collate_fn)

        # move model to device
        self.model.to(device)

        # get the trainable model parameters (weights)
        weights = [p for p in self.model.parameters() if p.requires_grad]
        optimizer = self.get_optimizer(weights)

        # create a learning rate scheduler
        lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                                       step_size=self.params['step_size'],
                                                       gamma=self.params['gamma'])
        res = None
        for epoch in range(self.params['epochs']):

            # train for one epoch, printing progress every n iterations
            _, self.losses = train_one_epoch(self.model, optimizer, data_loader_train, device, epoch,
                                             print_freq=self.params['print_frequency'], loss_stats=self.losses)
            # update the learning rate
            lr_scheduler.step()

            # evaluate on the validation dataset
            stats = evaluate(self.model, data_loader_val, device)
            # keep track of training progress
            self.track_stats(epoch, stats)

            # evaluate on the test dataset
            test_stats = evaluate(self.model, data_loader_test, device)
            # keep track of training progress
            self.track_test_stats(epoch, test_stats)

            # save snapshot of model every n epochs
            if 'save_frequency' in self.params and \
                    epoch % max(self.params['save_frequency'], 0) == 0:
                self.save_model(epoch)

        self.save_model(self.params['epochs'])
        self.save_stats()
        self.save_test_stats()
        self.save_losses()

        return self.model

    def get_optimizer(self, weights):
        """Instantiates an optimizer."""

        optimizer = None
        if self.params['optimizer'] == 'SGD':
            optimizer = torch.optim.SGD(weights, lr=self.params['lr'],
                                        momentum=self.params['momentum'],
                                        weight_decay=self.params['weight_decay'])
        elif self.params['optimizer'] == 'Adam':
            optimizer = torch.optim.Adam(weights, lr=self.params['lr'],
                                         weight_decay=self.params['weight_decay'])
        return optimizer

    def save_model(self, epoch):
        """Saves the model and the params to the model repository.
           Epoch is the current epoch in the training loop."""

        d = self.params['model_repo']
        if not os.path.exists(d):
            os.makedirs(d)
        if os.path.isdir(d):
            self.model_name = f'{self.params["model_repo"]}/{self.params["experiment"]}-' \
                              f'epoch{epoch}-' \
                              f'{datetime.datetime.now():%Y%m%d-%H%M}'
            print(f'Save model to {self.model_name}')
            torch.save(self.model, self.model_name)
            meta_name = self.model_name + ".json"
            print(f'Save model meta data to {meta_name}')
            with open(meta_name, "w") as f:
                f.write(json.dumps(self.params, indent=2))
        else:
            raise RuntimeError(f"Target folder for model not a directory ({self.params['model_repo']})")

    def load_model(self, path):
        """Loads a model from the given path."""

        # for safety load model to cpu, as we might not have a gpu.
        self.model = torch.load(path, map_location=torch.device('cpu'))
        return self.model

    def track_stats(self, epoch, stats):
        """Keeps track of training progress. Stats can be used to plot progress."""
        for iou_type, coco_eval in stats.coco_eval.items():
            if iou_type in self.stats:
                nd = self.stats[iou_type]
                r = np.hstack([epoch, coco_eval.stats])
                nd = np.vstack([nd, r])
            else:
                nd = np.hstack([epoch, coco_eval.stats])
            self.stats[iou_type] = nd

    def save_stats(self):
        """Saves collected stats to file."""

        if os.path.isdir(self.params['model_repo']):
            file_name = os.path.join(self.params['model_repo'], f'{self.model_name}.metrics')
            print(f'Save metrics to {file_name}')
            with open(file_name, "wb") as f:
                np.savez(f, bbox=self.stats['bbox'], segm=self.stats['segm'])
        else:
            raise RuntimeError(f"Target folder for model not a directory ({self.params['model_repo']})")

    def track_test_stats(self, epoch, stats):
        """Keeps track of training progress. Stats can be used to plot progress."""
        for iou_type, coco_eval in stats.coco_eval.items():
            if iou_type in self.test_stats:
                nd = self.test_stats[iou_type]
                r = np.hstack([epoch, coco_eval.stats])
                nd = np.vstack([nd, r])
            else:
                nd = np.hstack([epoch, coco_eval.stats])
            self.test_stats[iou_type] = nd

    def save_test_stats(self):
        """Saves collected stats to file."""

        if os.path.isdir(self.params['model_repo']):
            file_name = os.path.join(self.params['model_repo'], f'{self.model_name}.test-metrics')
            print(f'Save test metrics to {file_name}')
            with open(file_name, "wb") as f:
                np.savez(f, bbox=self.test_stats['bbox'], segm=self.test_stats['segm'])
        else:
            raise RuntimeError(f"Target folder for model not a directory ({self.params['model_repo']})")

    def save_losses(self):
        """Saves collected losses to file."""

        if os.path.isdir(self.params['model_repo']):
            file_name = os.path.join(self.params['model_repo'], f'{self.model_name}.losses')
            print(f'Save losses to {file_name}')
            with open(file_name, "wb") as f:
                np.savez(f, losses=self.losses)
        else:
            raise RuntimeError(f"Target folder for model not a directory ({self.params['model_repo']})")
