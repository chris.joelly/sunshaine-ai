from albumentations.core.transforms_interface import DualTransform, ImageOnlyTransform
from albumentations.augmentations.bbox_utils import convert_bbox_from_albumentations, convert_bbox_to_albumentations
import numpy as np
from PIL import Image
import os
import fnmatch
import random


def create_random_image(height, width):
    a = np.random.rand(height, width, 3) * 255
    return Image.fromarray(a.astype('uint8')).convert('RGB')


class EmbedToRandomImage(DualTransform):
    """Embed an image into a bigger image which is created from random pixels.
    Args:
        image_size: (Tuple) (WxH) The result image size where the image should be embedded to.
    Targets:
        image
    Image types:
        uint8 (shape: HxW | HxWxC)
    """
    def __init__(
            self,
            image_size=(640, 512),
            always_apply=False,
            p=1,
    ):
        super().__init__(always_apply=always_apply, p=p)

        assert len(image_size) == 2
        self.image_size = image_size

    def apply(self, image, **params):
        image_width, image_height = params["cols"], params["rows"]
        embed_pos_y, embed_pos_x = params["embed_pos"][0], params["embed_pos"][1]

        target_img = create_random_image(self.image_size[1], self.image_size[0])
        target = np.asarray(target_img)
        target[embed_pos_y:embed_pos_y + image_height, embed_pos_x:embed_pos_x + image_width, :] = image[:]

        return target

    def apply_to_mask(self, img: np.ndarray, **params) -> np.ndarray:
        image_width, image_height = params["cols"], params["rows"]
        embed_pos_y, embed_pos_x = params["embed_pos"][0], params["embed_pos"][1]

        mask = np.zeros([self.image_size[1], self.image_size[0]], dtype=np.uint8)
        mask[embed_pos_y:embed_pos_y + image_height, embed_pos_x:embed_pos_x + image_width] = 1
        mask = np.array(mask)

        return mask

    def apply_to_bbox(self, bbox, **params):
        bbox = convert_bbox_from_albumentations(bbox, 'pascal_voc',
                                                params["rows"], params["cols"], check_validity=True)
        bbox = (
            bbox[0] + params["embed_pos"][1],
            bbox[1] + params["embed_pos"][0],
            bbox[2] + params["embed_pos"][1],
            bbox[3] + params["embed_pos"][0]
        )
        bbox = convert_bbox_to_albumentations(bbox, 'pascal_voc',
                                              params["embed_size"][1], params["embed_size"][0], check_validity=True)
        return bbox

    def apply_to_keypoint(self, keypoint, **params):
        raise NotImplementedError("Method apply_to_keypoint is not implemented in class " + self.__class__.__name__)

    def get_params_dependent_on_targets(self, params):
        """Calculate and remember params which are relevant for all target transformations."""
        w, h = self.image_size[0], self.image_size[1]
        x, y = (self.image_size[0] - params["image"].shape[1]) // 2, \
               (self.image_size[1] - params["image"].shape[0]) // 2
        return {
            "embed_size": (w, h),
            "embed_pos": (y, x)
        }

    @property
    def targets_as_params(self):
        return ["image"]

    def get_transform_init_args_names(self):
        return ["image_size"]


class EmbedToRandomOekosolarImage(DualTransform):
    """Embed an image into a bigger image which is randomly taken from the Oekosolar infrared images.
    Args:
        image_folder: (str) The folder where the images to embed to are stored.
        pattern: (str) A pattern mask for filtering files from the image_folder.
    Targets:
        image
    Image types:
        uint8 (shape: HxW | HxWxC)
    """
    def __init__(
            self,
            image_folder,
            pattern='*',
            target_to_grayscale=False,
            always_apply=False,
            p=1,
    ):
        super().__init__(always_apply=always_apply, p=p)

        assert image_folder is not None
        assert os.path.isdir(os.path.join(image_folder))

        self.image_folder = image_folder

        self.image_files = [name for name in os.listdir(self.image_folder)
                            if os.path.isfile(os.path.join(self.image_folder, name))
                            and fnmatch.fnmatch(name, pattern)]

        self.pattern = pattern

        self.target_to_grayscale = target_to_grayscale

    def apply(self, image, **params):
        image_width, image_height = params["cols"], params["rows"]
        embed_pos_y, embed_pos_x = params["embed_pos"][0], params["embed_pos"][1]

        target_img = params["target_image"]
        if self.target_to_grayscale:
            target_img[embed_pos_y:(embed_pos_y + image_height), embed_pos_x:(embed_pos_x + image_width)] = image[:]
        else:
            for i in range(target_img.shape[2]):
                target_img[embed_pos_y:(embed_pos_y + image_height), embed_pos_x:(embed_pos_x + image_width), i] = image[:]

        return target_img

    def apply_to_mask(self, img: np.ndarray, **params) -> np.ndarray:
        image_width, image_height = params["cols"], params["rows"]
        embed_pos_y, embed_pos_x = params["embed_pos"][0], params["embed_pos"][1]

        mask = np.zeros([params["embed_size"][1], params["embed_size"][0]], dtype=np.uint8)
        mask[embed_pos_y:(embed_pos_y + image_height), embed_pos_x:(embed_pos_x + image_width)] = 1
        mask = np.array(mask)

        return mask

    def apply_to_bbox(self, bbox, **params):
        bbox = convert_bbox_from_albumentations(bbox, 'pascal_voc',
                                                params["rows"], params["cols"], check_validity=True)
        bbox = (
            bbox[0] + params["embed_pos"][1],
            bbox[1] + params["embed_pos"][0],
            bbox[2] + params["embed_pos"][1],
            bbox[3] + params["embed_pos"][0]
        )
        bbox = convert_bbox_to_albumentations(bbox, 'pascal_voc',
                                              params["embed_size"][1], params["embed_size"][0], check_validity=True)
        return bbox

    def apply_to_keypoint(self, keypoint, **params):
        raise NotImplementedError("Method apply_to_keypoint is not implemented in class " + self.__class__.__name__)

    def get_params_dependent_on_targets(self, params):
        """Calculate and remember params which are relevant for all target transformations."""
        target_file = random.sample(self.image_files, 1)[0]
        target_image = Image.open(os.path.join(self.image_folder, target_file))
        if self.target_to_grayscale:
            target_image = target_image.convert('L')
        target_image = np.asarray(target_image)
        width, height = target_image.shape[1], target_image.shape[0]
        x, y = (width - params["image"].shape[1]) // 2, \
               (height - params["image"].shape[0]) // 2
        return {
            "embed_size": (width, height),
            "embed_pos": (y, x),
            "target_image": target_image,
            "target_file": target_file
        }

    @property
    def targets_as_params(self):
        return ["image"]

    def get_transform_init_args_names(self):
        return ["image_folder", "pattern", "target_to_grayscale"]

