import sys
import os
import io
import platform
import subprocess as sp
from pathlib import Path
import numpy as np
import cv2 as cv
from PIL import Image
import matplotlib.pyplot as plt
from numba import jit

# add local 'external components' path to sys.path for import
local_libs = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.join(local_libs, "external"))


def extract_temp_data(thermal_image_file):
    os_name = "windows" if "win" in sys.platform else "linux"
    architecture_name = "release_x64" if "64bit" in platform.architecture()[0] else "release_x86"
    dji_binary = "dji_irp.exe" if "win" in sys.platform else "dji_irp"
    path_executable = str(Path("external/dji_thermal_sdk", os_name, architecture_name))

    if "linux" in os_name:
        # Linux library loader needs path to libdirp.so
        os.environ["LD_LIBRARY_PATH"] = path_executable

    temp_data_file = "output.temp"
    sp.run(
        [str(Path(path_executable, dji_binary)),
         "-a", "measure", "--measurefmt", "float32", "--distance", "25", "-s", thermal_image_file,
         "-o", temp_data_file],
        universal_newlines=True,
        stdout=sp.DEVNULL,
        stderr=sp.STDOUT,
    )
    temp_data = Path(temp_data_file).read_bytes()
    data = np.frombuffer(temp_data, dtype=np.float32, count=-1).reshape((512, 640))

    os.remove(temp_data_file)

    return data


@jit(nopython=True)
def move_to_new_mean(temp_data, new_mean):
    mean = np.mean(temp_data)
    t = temp_data.copy()
    t += (new_mean - mean)
    return t


@jit(nopython=True)
def normalize_data(data, min_=None, max_=None):
    if min_ is None or max_ is None:
        min_ = np.amin(data)
        max_ = np.amax(data)
    t = (data - min_)/(max_ - min_)
    return t


@jit(nopython=True)
def standardize_data(data, mean=None, std=None):
    if mean is None or std is None:
        mean = np.mean(data)
        std = np.std(data)
    t = (data - mean) / std
    return t


@jit(nopython=True)
def center_data(data, mean=None):
    if mean is None:
        mean = np.mean(data)
    t = data - mean
    return t


def clip_temp_data(data, min_thresh=0, max_thresh=100):
    t = data.copy()
    t[t < min_thresh] = min_thresh
    t[t > max_thresh] = max_thresh
    return t


def min_max_scale(data, min_temp=None, max_temp=None):
    """Normalize a temperature values to the 0-255 uint8 range."""
    if min_temp is not None and max_temp is not None:
        min_ = min_temp
        max_ = max_temp
    else:
        min_ = np.amin(data)
        max_ = np.amax(data)
    num = data - min_
    den = max_ - min_
    t = np.array((num / den) * 255, dtype=np.uint8)
    return t


@jit(nopython=True)
def clip_negative_values(temp_data):
    t = temp_data.copy()
    t = np.where(t < 0, 0, t)
    return t


def get_greyscale_temp_image(temp_data, min_temp=None, max_temp=None):
    temp_data_bytes = min_max_scale(temp_data, min_temp, max_temp)
    identity = np.arange(256, dtype=np.dtype('uint8'))
    lut = np.dstack((identity, identity, identity))
    i = np.dstack((temp_data_bytes, temp_data_bytes, temp_data_bytes))
    thermal_image = cv.LUT(i, lut)
    return thermal_image


def generate_colorbar(min_temp=None, max_temp=None, cmap=cv.COLORMAP_BONE, height=None):
    # Build a 255 x 21 colour bar, and create an image to append to the main image
    cb_gray = np.arange(255, 0, -1, dtype=np.uint8).reshape((255, 1))
    cb_gray = np.tile(cb_gray, 21)
    cb_color = cv.applyColorMap(cb_gray, cmap) if cmap is not None else cv.cvtColor(cb_gray, cv.COLOR_GRAY2BGR)

    # Decide layout of the colourbar canvas
    cb_canvas_height_slice = height if height is not None else 255
    cb_canvas = np.zeros((cb_canvas_height_slice, cb_color.shape[1] + 30, 3), dtype=np.uint8)
    cb_canvas_start_height = cb_canvas.shape[0] // 2 - cb_color.shape[0] // 2
    cb_height_slice = slice(cb_canvas_start_height, cb_canvas_start_height + cb_color.shape[0])

    # Populate the canvas with the colourbar and relevant text
    cb_canvas[cb_height_slice, 10: 10 + cb_color.shape[1]] = cb_color
    min_temp_text = (5, cb_canvas_start_height + cb_color.shape[0] + 30)
    max_temp_text = (5, cb_canvas_start_height - 20)
    cv.putText(cb_canvas, str(round(min_temp, 2)), min_temp_text, cv.FONT_HERSHEY_PLAIN, 1, (255, 0, 0), 1, 8)
    cv.putText(cb_canvas, str(round(max_temp, 2)), max_temp_text, cv.FONT_HERSHEY_PLAIN, 1, (0, 0, 255), 1, 8)

    return cb_canvas


def add_color_bar(thermal_img, cmap=cv.COLORMAP_BONE, min_temp=None, max_temp=None):
    # Generate a colour bar and add it to the cmapped temp image
    cb_canvas = generate_colorbar(cmap=cmap, min_temp=min_temp, max_temp=max_temp)
    thermal_img = cv.hconcat((thermal_img, cb_canvas))
    # Convert from CV colour coding to PIL colour coding
    thermal_img = cv.cvtColor(thermal_img, cv.COLOR_BGR2RGB)
    thermal_img = Image.fromarray(thermal_img)
    return thermal_img


def save_thermal_image(temp_data, output_path, image_name):
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    p = os.path.join(output_path, os.path.basename(image_name))
    temp_data = cv.cvtColor(temp_data, cv.COLOR_BGR2RGB)
    thermal_img = Image.fromarray(temp_data)
    thermal_img.save(p, quality=100, subsampling=0)
    return thermal_img


def get_dataset_statistics(temp_values):
    min_ = np.amin(temp_values)
    max_ = np.amax(temp_values)
    mean_ = np.mean(temp_values)
    median_ = np.median(temp_values)
    std_ = np.std(temp_values)
    return min_, max_, mean_, median_, std_


def save_histogram(file_name, title, xtitle, scale, values, mean=None, xlim=None):
    # prepare folder for histogram
    if xlim is None:
        xlim = [-5, 105]
    p, f = os.path.split(file_name)
    if not os.path.exists(p):
        os.makedirs(p)
    # extract image file type
    filetype = os.path.splitext(f)[1][1:]
    if mean is None:
        mean = np.mean(values)
    plt.figure()
    plt.hist(x=values, bins=100)
    plt.title(title)
    plt.xlabel(xtitle)
    plt.xlim(xlim)
    plt.ylabel("Occurrences")
    plt.yscale(scale)
    # add line marker for mean
    plt.axvline(x=mean, color='r', label='mean')
    # add legend
    plt.legend(loc='upper right', ncol=1, borderaxespad=2)
    # render figure to image
    img_buf = io.BytesIO()
    plt.savefig(img_buf, format=filetype)
    histogram = Image.open(img_buf)
    histogram.save(file_name)
    img_buf.close()
    plt.close()
    return histogram
