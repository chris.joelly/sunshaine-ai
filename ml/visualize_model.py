import math
import os
import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import cv2 as cv
import argparse
from torchvision import transforms
from torchvision.models import resnet50
from torchvision.models.feature_extraction import get_graph_node_names
from torchvision.models.feature_extraction import create_feature_extractor
from torchvision.models.detection.mask_rcnn import MaskRCNN
from torchvision.models.detection.backbone_utils import LastLevelMaxPool
from torchvision.ops.feature_pyramid_network import FeaturePyramidNetwork

def load_model(path):
    model = torch.load(path)
    #model = models.resnet50(pretrained=True)
    print(model)
    return model

def extract_kernels_and_conv_layers(module):
    module_children = list(module.children())
    module_weights, conv_layers = [], []
    get_conv_layers(module_children, module_weights, conv_layers)
    return module_weights, conv_layers

def get_conv_layers(model, weights, layers):
    for i in range(len(model)):
        curr_layer = model[i]
        if type(curr_layer) == nn.Conv2d:
            weights.append(curr_layer.weight)
            layers.append(curr_layer)
        else:
            c = list(curr_layer.children())
            if len(c) > 0:
                get_conv_layers(c, weights, layers)

def read_image(path):
    img = cv.imread(path)
    img = cv.cvtColor(img, cv.COLOR_BGR2RGB)
    return img

def preprocess_image(img):
    # define the transforms
    transform = transforms.Compose([
        transforms.ToPILImage(),
        #transforms.Resize((512, 512)),
        transforms.ToTensor(),
    ])
    img = np.array(img)
    img = transform(img)
    # unsqueeze to add a batch dimension
    img = img.unsqueeze(0)
    return img

def save_layer_filters_to_image(folder, layer_no, layer_name, weights):

    def get_plot_dim(weights):
        """Calculates the size of the filter grid so that all filters can be visualized in a rectangular grid."""
        dim = np.sqrt(weights.shape[0])
        (f, i) = math.modf(dim)
        if f == 0:
            dim = (int(i), int(i))
        elif f <= 0.5:
            dim = (int(i) + 1, int(i))
        else:
            dim = (int(i) + 1, int(i) + 1)
        return dim

    if not os.path.exists(folder) or not os.path.isdir(folder):
        os.makedirs(folder)

    # get dimensions of layer to get a square shaped filter grid
    dim = get_plot_dim(weights)

    filtermaps = weights.shape[1]

    for filtermap in range(filtermaps):

        fig = plt.figure(figsize=(30, 30))
        fig.suptitle(f'Layer {layer_no}: {layer_name}', fontsize=36)

        for i, filter in enumerate(weights):
            plt.subplot(dim[0], dim[1], i + 1)
            t = filter[filtermap, :, :].cpu().detach().numpy()
            plt.imshow(t, cmap='gray')
            plt.axis('off')

        plt.savefig(os.path.join(folder, f'filter-{layer_no}-{filtermap}.png'))
        plt.close(fig)

def save_filter_images(image_folder, model_weights, conv_layers):
    for layer_no, (weight, conv) in enumerate(zip(model_weights, conv_layers)):
        print(f"Saving layer {layer_no} filters: {conv} {weight.shape}...")
        save_layer_filters_to_image(image_folder, layer_no, conv, weight)

def extract_featuremaps(img, conv_layers):
    skipped_layers = []  # due to other layers, e.g. maxpool, etc., conv layers might not match dimensions
    # pass the image through all the layers and collect results
    print(f'{0}: {conv_layers[0]}')
    results = [conv_layers[0](img)]
    for i in range(1, len(conv_layers)):
        # pass the result from the last layer to the next layer
        skipped = False
        try:
            if results[-1].shape[1] == conv_layers[i].in_channels:
                results.append(conv_layers[i](results[-1]))
            else:
                skipped = True
                skipped_layers.append(i)
            print(f'Extract featuremap layer {i}: {conv_layers[i]}'
                  f'{" : SKIPPED due to dimension mismatch at input!" if skipped else ""}')
        except RuntimeError as e:
            print(e)

    skipped_layers.sort(reverse=True)
    for i in skipped_layers:
        del(conv_layers[i])

    return results

def save_featuremaps_images(folder, featuremaps, conv_layers):

    def get_plot_dim(featuremap):
        """Calculate the size of the featuremap grid so that all featuremaps can be visualized in a rectangular grid."""
        dim = np.sqrt(featuremap.shape[1])
        (f, i) = math.modf(dim)
        if f == 0:
            dim = (int(i), int(i))
        elif f <= 0.5:
            dim = (int(i) + 1, int(i))
        else:
            dim = (int(i) + 1, int(i) + 1)
        return dim

    if not os.path.exists(folder) or not os.path.isdir(folder):
        os.makedirs(folder)

    # visualize features from each layer
    for layer_no, (featuremap, conv) in enumerate(zip(featuremaps, conv_layers)):

        fig = plt.figure(figsize=(30, 30))
        fig.suptitle(f'Layer {layer_no}: {conv}', fontsize=36)
        dim = get_plot_dim(featuremap)
        layer_viz = featuremap[0, :, :, :]
        layer_viz = layer_viz.data
        print(f"Saving layer {layer_no} feature maps {layer_viz.size()}...")
        for i, filter in enumerate(layer_viz):
            plt.subplot(dim[0], dim[1], i + 1)
            t = filter.cpu().detach().numpy()
            plt.imshow(t, cmap='gray')
            plt.axis("off")

        plt.savefig(os.path.join(folder, f'featuremap-{layer_no}.png'))
        plt.close(fig)

# MaskRCNN requires a backbone with an attached FPN
class Resnet50WithFPN(torch.nn.Module):
    def __init__(self):
        super(Resnet50WithFPN, self).__init__()
        # Get a resnet50 backbone
        m = resnet50()
        # Extract 4 main layers (note: MaskRCNN needs this particular name
        # mapping for return nodes)
        self.body = create_feature_extractor(m, return_nodes={f'layer{k}': str(v) for v, k in enumerate([1, 2, 3, 4])})
        # Dry run to get number of channels for FPN
        inp = torch.randn(2, 3, 224, 224)
        with torch.no_grad():
            out = self.body(inp)
        in_channels_list = [o.shape[1] for o in out.values()]
        # Build FPN
        self.out_channels = 256
        self.fpn = FeaturePyramidNetwork(
            in_channels_list, out_channels=self.out_channels,
            extra_blocks=LastLevelMaxPool())

    def forward(self, x):
        x = self.body(x)
        x = self.fpn(x)
        return x


if __name__ == '__main__':

    ap = argparse.ArgumentParser()
    ap.add_argument('-i', '--input', required=True, help='path to input image')
    ap.add_argument('-o', '--output', required=True, help='path to output folder')
    ap.add_argument('-m', '--model', required=True, help='path to model')
    args = ap.parse_args()

    model = load_model(args.model)

    model_weights, conv_layers = extract_kernels_and_conv_layers(model)

    save_filter_images(args.output, model_weights, conv_layers)

    img = read_image(args.input)
    img = preprocess_image(img)

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    img = img.to(device)
    model = model.to(device)

    featuremaps = extract_featuremaps(img, conv_layers)

    save_featuremaps_images(args.output, featuremaps, conv_layers)

    # train_nodes, eval_nodes = get_graph_node_names(model.backbone)
    #
    # nodes = {
    #     "body.conv1": "layer1",
    #     "body.maxpool": "layer2",
    # }
    # backbone = create_feature_extractor(model.backbone, return_nodes=nodes)
    # model.backbone.body = backbone
    # model.eval()

    # model = MaskRCNN(Resnet50WithFPN(), num_classes=91).eval()
    # model.to("cuda")
    #
    # out = model(img.cuda())


