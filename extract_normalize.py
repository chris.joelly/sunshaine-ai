import argparse
import os
import numpy as np
import cv2 as cv
from PIL import Image

from ml.preprocessing import extract_temp_data, save_thermal_image, get_greyscale_temp_image, \
    standardize_data, center_data, normalize_data, move_to_new_mean, get_dataset_statistics
from myutils.utils import init_dest_folder

# different types of preprocessing the image data
CENTER = "center"
STANDARDIZE_LOCAL = "local_standardize"
STANDARDIZE_GLOBAL = "standardize"
NORMALIZE = "normalize"
FIXED_MEAN = "fixed-mean"
PROCESSING_TYPES = [CENTER, STANDARDIZE_LOCAL, STANDARDIZE_GLOBAL, NORMALIZE, FIXED_MEAN]

fixed_mean = 40

# values calculated from Oekosolar thermal dataset for model training
target_mean = 12.427
target_std = 5.270
target_min = -10.8
target_max = 21.6

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Standard normalization of image data.')
    parser.add_argument('-i', '--input', type=str, required=True,
                        help='Name of folder containing images to normalize')
    parser.add_argument('-o', '--output', type=str, required=True,
                        help='Name of folder where normalized images are stored')
    parser.add_argument('--temp', action=argparse.BooleanOptionalAction,
                        help='When set, images from extracted temperatures are stored')
    parser.add_argument('-t', '--type', type=str, required=True,
                        help='Type of preprocessing. One of: (center, local_standardize, '
                             'standardize, normalize, fixed-mean)')
    args = parser.parse_args()

    assert args.type in PROCESSING_TYPES, f"Unknown processing type specified: {args.type}."
    assert os.path.isdir(args.input), f"{args.input} is not a folder."

    init_dest_folder(args.output)
    if args.temp:
        temp_folder = init_dest_folder(os.path.join(args.output, "temp"))

    # --------------------------------------------------------------------------------------
    # Read files of dataset

    print(f"Processing folder {args.input}")

    files = os.listdir(args.input)
    files = [f for f in files if os.path.isfile(os.path.join(args.input, f))]

    # --------------------------------------------------------------------------------------
    # Extract temperature values from thermal images

    all_temp_values = None

    temp_data = {}
    for file in files:

        print(f"Extracting temperature from file {os.path.join(args.input, file)}", end="")

        temp_data[file] = extract_temp_data(os.path.join(args.input, file))

        min_temp, max_temp, mean_temp, median_temp, std_temp = get_dataset_statistics(temp_data[file])

        print(f': min/max/mean/std/median : {min_temp:>5.1f}/{max_temp:>5.1f}/'
              f'{mean_temp:>5.1f}/{std_temp:>5.1f}/{median_temp:>5.1f}')

        if args.temp:
            img = get_greyscale_temp_image(temp_data[file], min_temp=min_temp, max_temp=max_temp)
            save_thermal_image(img, temp_folder, f'{file}.png')

        if all_temp_values is None:
            all_temp_values = temp_data[file]
            all_temp_values = all_temp_values.reshape((-1, 512, 640))
        else:
            all_temp_values = np.vstack((all_temp_values, [temp_data[file]]))

    dataset_min, dataset_max, dataset_mean, dataset_median, dataset_std = get_dataset_statistics(all_temp_values)
    all_temp_values_tmp = all_temp_values.copy()

    # --------------------------------------------------------------------------------------
    # Standardization of temperature data

    if args.type == STANDARDIZE_GLOBAL:
        print(f"Standardizing to mean/std {target_mean:>5.3f}/{target_std:>5.3f}...")
        all_temp_values_tmp = standardize_data(all_temp_values_tmp, target_mean, target_std)
    elif args.type == STANDARDIZE_LOCAL:
        print("Standardizing per image...")
        all_temp_values_tmp = standardize_data(all_temp_values_tmp)
    elif args.type == CENTER:
        print("Centering...")
        all_temp_values_tmp = center_data(all_temp_values_tmp, dataset_mean)
    elif args.type == NORMALIZE:
        print("Normalizing...")
        all_temp_values_tmp = normalize_data(all_temp_values_tmp, dataset_min, dataset_max)
    elif args.type == FIXED_MEAN:
        print(f"Normalizing to mean {fixed_mean:>5.3f}")
        all_temp_values_tmp = move_to_new_mean(all_temp_values_tmp, fixed_mean)

    min_temp, max_temp, mean_temp, median_temp, std_temp = get_dataset_statistics(all_temp_values_tmp)
    print(f'Dataset min/max/mean/std/median : {min_temp:>5.1f}/{max_temp:>5.1f}/'
          f'{mean_temp:>5.1f}/{std_temp:>5.1f}/{median_temp:>5.1f}')

    # get temperature scale depending on normalization type
    if args.type == STANDARDIZE_GLOBAL:
        min_, max_ = target_min, target_max
    else:
        min_, max_ = min_temp, max_temp

    # --------------------------------------------------------------------------------------
    # Save transformed dataset

    i = 0
    for file in files:
        print(f"Saving file {os.path.join(args.output, file)}")
        img = get_greyscale_temp_image(all_temp_values_tmp[i], min_temp=min_, max_temp=max_)
        save_thermal_image(img, args.output, f'{file}.png')
        i += 1

    # --------------------------------------------------------------------------------------
    # Transfer EXIF data to preprocessed images

    for file in files:
        print(f"Transferring EXIF data to file {os.path.join(args.output, file)}")

        image = Image.open(os.path.join(args.input, file))
        exif = image.info['exif']

        f = os.path.join(args.output, f'{file}.png')  # use PNG to avoid JPEG degradation
        image = Image.open(f)
        os.remove(f)
        image.save(os.path.join(args.output, file), exif=exif, quality=100, subsampling=0)
