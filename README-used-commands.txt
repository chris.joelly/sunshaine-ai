
# When setting up the project "SunshAIne", consider the following:
#
# Consider using the following env variables, adapt them to your storage needs
#
#    $SUNSHAINE_ROOT           # source and tools of sunshaine-ai prototype.
#    $SUNSHAINE_MODEL_REPO     # folder with trained models and their stats.
#    $SUNSHAINE_DATASETS       # folder with thermal image datasets.
#    $SUNSHAINE_DETECTOR_WF    # workspace of detector workflow with results

# Below listed commands are used to:
#
#   * run experiments with training the model/detector,
#   * test the model on test images,
#   * create plots and stats tables from training statistics,
#   * annotate test dataset with detections and create confusion matrices and result plots over all experiments
#
# each command prints its possible parameters when called without parameter
# all command calls are noted relative to $SUNSHAINE_ROOT

# install a Python 3.9 virtual environment in $SUNSHAINE_ROOT, activate it and install packages from requirements.txt:
# pip install -r requirements.txt
#
# Enable CUDA by installing NVIDIA CUDA and cuCNN libs.
# This prototype was developed with CUDA 11.3, torch 1.13.0, torchvision 0.14.0

# ----------------------------------------------------------------------------------------------------------------------

# batch clone all paramsets in to case the params will be edited to create new experiments
for i in paramsets/exp*; do cp $i `basename $i .json`-A20.json; done

# batch modify strings in cloned paramsets
sed -i "s/A10/A20/g" *A20.json
sed -i "s/augmented/augmented-20/g" *A20.json


# extend dataset by augmentation (change annotation file name in paramsets, augmentation creates new annotations!)
PYTHONPATH=./ python3 scratchpad/dataset_expander.py -e plant2-stdnorm-augmented \
-if $SUNSHAINE_DATASETS/plant2/normalized-nogrey \
-ia $SUNSHAINE_DATASETS/plant2/instances_default.json \
-of $SUNSHAINE_DATASETS/plant2/normalized-nogrey-augmented \
-oa $SUNSHAINE_DATASETS/plant2/normalized-nogrey-augmented/instances_default.json \
--passes 20

# run experiments for given parmeter sets
for p in $(ls paramsets/exp-plant2-*A20*); do p=$(basename $p .json); echo "Running: $p"; python3 -u train-model.py -c paramsets/$p.json | stdbuf -oL tee logs/$p.log; done

# standard normalize thermal images from vendor DJI (consider when images from different camera vendor!)
PYTHONPATH=./ python3 -u ./extract_normalize.py \
--input=$SUNSHAINE_DATASETS/plant2/images
--output=$SUNSHAINE_DATASETS/plant2/normalized-nogrey
--type=standardize
--temp

# test model and create side-by-side images
PYTHONPATH=./ python3 -u ./test-model.py --model $SUNSHAINE_MODEL_REPO/exp-plant2-7-100-SGD-A20-epoch100-20221226-0127 \
$SUNSHAINE_DATASETS/plant2/normalized-nogrey/test

# analyse detections
# pass test dataset through models and annotate images with detections and create confusion matrix and overall AP/AR/F1 plots for each model.
# the annotations for the test dataset are taken from the whole annotation file.
PYTHONPATH=./ python3 -u scratchpad/analyse_detections.py --model_repo=$SUNSHAINE_MODEL_REPO \
--model_pattern exp-plant2-.*\.losses \
--annotations=$SUNSHAINE_DATASETS/plant2/instances_default.json \
$SUNSHAINE_DATASETS/plant2/normalized-nogrey/test | stdbuf -oL tee logs/analyse-detections.log

# read stats from training sessions and create loss and AP/AR metrics plots as well ax Latex tables for each experiment
PYTHONPATH=./ python3 -u scratchpad/process-stats.py --model_repo=$SUNSHAINE_MODEL_REPO \
--model_pattern=exp-plant2-.*Adam.*\.losses | stdbuf -oL tee logs/process-stats.log

# ----------------------------------------------------------------------------------------------------------------------

# Commands for the detector workflow for anomaly detection and localization

# The orthophoto (georeferenced GeoTIFF) needs to be create before running the workflow.
# Check ODM (e.g. WebODM Docker image).
# Put orthophoto to folder e.g. /home/user/sunshaine/detector-workflow
# Results are placed into same folder

# Preprocessing of the given orthophoto (.tif). Results are saved to folder $SUNSHAINE_DETECTOR_WF
PYTHONPATH=./ python3 -u detection-preprocess.py \
--folder $SUNSHAINE_DETECTOR_WF

# Perform anomaly detection on the patches of the orthophotos. Results are saved to folder $SUNSHAINE_DETECTOR_WF/patches
PYTHONPATH=./ python3 -u detection.py \
--model $SUNSHAINE_MODEL_REPO/exp-plant2-9-200-SGD-A20-epoch200-20221227-0457 \
$SUNSHAINE_DETECTOR_WF/patches

# Post-processing of the detection results. Results are saved to folder $SUNSHAINE_DETECTOR_WF
PYTHONPATH=./ python3 -u detection-postprocess.py \
--file_name $SUNSHAINE_DETECTOR_WF/Dahlienstrae-18-10-2021-orthophoto-standard-normalized.tif

# ----------------------------------------------------------------------------------------------------------------------

# Command for the failed RaptorMaps InfraredSolarModules
PYTHONPATH=./ python3 -u scratchpad/train-raptor-oeko-2class.py \
-c $SUNSHAINE_ROOT/paramsets/maskrcnn-raptor-on-oekosolar-gray-resize-laptop1.json
