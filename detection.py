import argparse
import sys
import os
from PIL import Image
import json

# add local 'external components' path to sys.path for import
local_libs = os.getcwd()
sys.path.append(os.path.join(local_libs, "external"))

import albumentations as A
import albumentations.pytorch.transforms as PA
import torch
import myutils
import cv2
import numpy as np

PATCHES_FOLDER = 'patches'
ANOMALY_META = "anomalies.json"
FILE_PATTERN = ("jpg", "png")


def get_cuda_device():
    if torch.cuda.is_available():
        print(f'GPU: {torch.cuda.get_device_name(0)}')
        device = torch.device('cuda')
        torch.cuda.empty_cache()
    else:
        device = torch.device('cpu')
    print(f'Device: {device}')
    return device


def center_from_contours(contours):
    centers = []
    for contour in contours:
        c = max(contour, key=len)
        M = cv2.moments(c)
        # print(M)
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])
        centers.append((cx, cy))
    return centers


def masks_to_contour(masks):
    contours = []
    masks_ = torch.from_numpy(masks)
    masks_ = masks_.ge(0.5).mul(255).byte().cpu().numpy()
    for m in masks_:
        ret, thresh = cv2.threshold(m[0], 127, 255, 0)
        c, h = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours.append(c)
    return contours


def detect(folder, model_name, score_threshold=0.5):

    model = torch.load(model_name, map_location=torch.device('cpu'))
    model.eval()

    device = get_cuda_device()
    model.to(device)

    transforms = A.Compose([
        A.ToFloat(max_value=255),
        PA.ToTensorV2(),
    ])

    image_files = [name for name in os.listdir(folder)
                   if os.path.isfile(os.path.join(folder, name))
                   and name.lower().endswith(FILE_PATTERN)]

    anomalies = {}

    for image_fn in image_files:

        img = Image.open(os.path.join(folder, image_fn)).convert('RGB')

        i = transforms(image=np.array(img))['image']
        if device.type == 'cuda':
            i = i.cuda()

        with torch.no_grad():
            predictions = model([i])

        if len(predictions) > 0 and \
                len(predictions[0]['scores'].detach().cpu().tolist()) > 0:
            p = read_from_device(predictions[0])
            idx = p['scores'] > score_threshold
            scores = p['scores'][idx]
            if len(scores) > 0:
                classes = p['labels'][idx]
                boxes = p['boxes'].byte()[idx]
                contours = masks_to_contour(p['masks'][idx])
                centers = center_from_contours(contours)
                anomalies[image_fn] = {
                    'boxes': boxes,
                    'classes': classes,
                    'scores': scores,
                    'contours': contours,
                    'centers': centers,
                }
    save_anomalies(folder, anomalies)

    return len(anomalies.keys())


def save_anomalies(folder, anomalies):
    class NumpyEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, np.ndarray):
                return obj.tolist()
            return json.JSONEncoder.default(self, obj)

    with open(os.path.join(folder, ANOMALY_META), 'w') as m:
        m.write(json.dumps(anomalies, indent=4, cls=NumpyEncoder))


def lambda_handler(event, context):
    assert event['file_name'] is not None and len(event['file_name']) > 0, "Missing param 'file_name'!"
    #assert event['model_name'] is not None and len(event['model_name']) > 0, "Missing param 'model_name'!"
    #assert event['score_threshold'] is not None and len(event['score_threshold']) > 0, "Missing param 'score_threshold'!"
    folder, name = os.path.split(event['file_name'])
    model_name = os.path.join("/mnt/sunshaine/model", "sunshaine.model")
    num_patches = detect(os.path.join(folder, PATCHES_FOLDER), model_name, 0.75)
    return {
        "file_name": event['file_name'],
        "patches": num_patches
    }


def read_from_device(prediction):
    for key, value in prediction.items():
        prediction[key] = value.to("cpu").numpy()
    return prediction


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Test instance segmentation.')
    parser.add_argument('folder', type=str, help='Folder with JSON annotation file and images in "images" folder.')
    parser.add_argument('--model', type=str, default='model', help='Filename of model to load.')
    parser.add_argument('--score', type=float, default=0.5, help='Min score to use for cutoff detections.')
    args = parser.parse_args()

    print(f'Test with data from: {args.folder}')
    print(f'Model: {args.model}')

    model = torch.load(args.model, map_location=torch.device('cpu'))
    print(model)

    model.eval()

    device = get_cuda_device()
    model.to(device)

    transforms = A.Compose([
        A.ToFloat(max_value=255),
        PA.ToTensorV2(),
    ])

    results_path = os.path.join(args.folder, 'results')
    if not os.path.exists(results_path):
        os.makedirs(results_path)

    image_files = [name for name in os.listdir(args.folder)
                   if os.path.isfile(os.path.join(args.folder, name))
                   and name.lower().endswith(FILE_PATTERN)]

    anomalies = {}

    for image_fn in image_files:

        print(f"Processing file {image_fn}: ", end="")

        img = Image.open(os.path.join(args.folder, image_fn)).convert('RGB')

        i = transforms(image=np.array(img))['image']

        with torch.no_grad():
            predictions = model([i.cuda()])

        score_threshold = args.score

        while True:
            pred_boxes = {
                'boxes': predictions[0]['boxes'].clone(),
                'labels': predictions[0]['labels'].clone(),
                'scores': predictions[0]['scores'].clone()
            }
            pred_masks = {
                'masks': predictions[0]['masks'].clone(),
                'labels': predictions[0]['labels'].clone(),
                'scores': predictions[0]['scores'].clone()
            }

            output_bbox, _, _ = myutils.show(img, [pred_boxes], score_threshold)
            output_mask, _, _ = myutils.show(img, [pred_masks], score_threshold)

            wait = 1  # put 0 for user interaction, >0 for delayed continuation
            if len(predictions) > 0 and \
                    len(predictions[0]['labels'].detach().cpu().tolist()) > 0:
                p = read_from_device(predictions[0])
                idx = p['scores'] > score_threshold
                scores = p['scores'][idx]
                if len(scores) > 0:
                    classes = p['labels'][idx]
                    boxes = p['boxes'][idx]
                    contours = masks_to_contour(p['masks'][idx])
                    centers = center_from_contours(contours)
                    print(f"p>{score_threshold:.2f} -> {sum(idx)} issues found\n"
                          f"  classes: {classes}\n"
                          f"  scores:  {scores}\n"
                          f"  boxes:   {boxes}\n"
                          f"  centers: {centers}")
                    anomalies[image_fn] = {
                        'boxes': boxes,
                        'classes': classes,
                        'scores': scores,
                        'contours': contours,
                        'centers': centers,
                    }

                    for center in centers:
                        output = cv2.circle(output_mask, center, radius=2, color=(0, 0, 255), thickness=-1)

                    cv2.imwrite(os.path.join(results_path, image_fn + '-result-mask.jpg') , output_mask)
                    cv2.imwrite(os.path.join(results_path, image_fn + '-result-bbox.jpg'), output_bbox)
                    img_corr = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2RGB)
                    # side_by_side = np.concatenate((img_corr, output), axis=1)
                    side_by_side = np.concatenate((output_bbox, output_mask), axis=1)
                    cv2.imwrite(os.path.join(results_path, image_fn + '-sidebyside.jpg'), side_by_side)

                    cv2.imshow('side-by-side', side_by_side)
                    k = cv2.waitKey(wait)
                    if k in [13, 32]:  # RETURN, SPACE
                        break
                    elif k == -1:
                        break
                    elif k == 43:  # +, plus
                        if score_threshold < 1.0:
                            score_threshold += 0.05
                    elif k == 45:  # -, minus
                        if score_threshold > 0.05:
                            score_threshold -= 0.05
                    elif k in (27, 113):  # ESC, q
                        quit(1)
                    else:
                        print(f"{k}: use "
                              f"('RETURN', 'SPACE') for next, "
                              f"('+', '-') for setting confidence val or "
                              f"('q', 'ESC') to quit")
                else:
                    print(f"  found objects below confidence value of {score_threshold}!")
                    break
            else:
                print("  nothing found!")
                break

    save_anomalies(args.folder, anomalies)
