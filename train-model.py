import argparse
import sys
import os
import json

import albumentations as A
import albumentations.pytorch.transforms as PA

# add local 'external components' path to sys.path for import
local_libs = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.join(local_libs, "external"))

from ml.ds import CocoLikeDataset
from ml.model import ModelTrainer


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Train instance segmentation.')
    parser.add_argument('-c', '--param_file', type=str, required=True,
                        help='Filename of JSON file with training parameters')
    args = parser.parse_args()

    print(f'Training with params from: {args.param_file}')

    # get parameters for training session
    with open(args.param_file, "r") as f:
        params = json.load(f)

    print(f'Config: \n{json.dumps(params, indent=4)}')

    # transform input data to float and tensor
    transforms = A.Compose([
        A.ToFloat(max_value=255),
        PA.ToTensorV2(),
    ], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels']))

    # load dataset
    dataset = CocoLikeDataset(name=params['experiment'],
                              root=params['images_folder'],
                              annFile=params['annotation_file'],
                              save_images=params['save_images'],
                              save_masks=params['save_masks'],
                              transforms=transforms)

    # load test dataset for model test
    test_dataset = CocoLikeDataset(name=params['experiment'],
                              root=params['test_images_folder'],
                              annFile=params['test_annotation_file'],
                              save_images=params['save_images'],
                              save_masks=params['save_masks'],
                              transforms=transforms)

    # get model trainer
    trainer = ModelTrainer(params=params)

    print(trainer.model)

    trainer.train_model(dataset, test_dataset, train_val_split=params['train_val_split'])
