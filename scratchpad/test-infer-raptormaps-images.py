import argparse
import sys
import os

# add local 'external components' path to sys.path for import
local_libs = os.getcwd()
sys.path.append(os.path.join(local_libs, "external"))

import PIL.Image
from ml.model import CustomMaskRCNNModelManager
from ml.ds import RaptorMaps2ClassDataset
import torch
import myutils
import cv2
import numpy as np

from torchvision.transforms import ToPILImage

import utils


def tensor_to_image(tensor):
    tensor = tensor*255
    tensor = np.array(tensor, dtype=np.uint8)
    if np.ndim(tensor)>3:
        assert tensor.shape[0] == 1
        tensor = tensor[0]
    return PIL.Image.fromarray(tensor)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Test instance segmentation.')
    parser.add_argument('folder', type=str, help='Folder with image dataset')
    parser.add_argument('--model', type=str, default='maskrcnn-raptor-instance.1', help='Filename of model')
    args = parser.parse_args()

    print(f'Test with images from: {args.folder}')
    print(f'Model: {args.model}')

    num_classes = 3  # test simple case: background, No-Anomaly, Anomaly

    modelMgr = CustomMaskRCNNModelManager(num_classes,
                                          pretrained=False,
                                          image_mean=(0, 0, 0), image_std=(1, 1, 1))
    model = modelMgr.load_model(args.model)
    model.eval()

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)

    # --------------------

    dataset = RaptorMaps2ClassDataset('Raptor', args.folder)
    data_loader = torch.utils.data.DataLoader(
        dataset, batch_size=1, shuffle=False, num_workers=4,
        collate_fn=utils.collate_fn)

    for image, target in data_loader:

        with torch.no_grad():
            predictions = model([image[0].cuda()])

        i = ToPILImage()(image[0]).convert('RGB')
        output, results, top = myutils.show(i, predictions, 0.3)

        result = cv2.resize(output, dsize=(8*24, 8*40), interpolation=cv2.INTER_CUBIC)
        cv2.imshow('result', result)
        cv2.waitKey(2)

        p = predictions[0]
        correct = p['labels'][0].to('cpu') == target[0]['labels'].to('cpu')
        print(f"{target[0]['image_id'][0]}: "
              f"{p['labels']}, {p['scores']}: "
              f"{p['labels'][0]}={p['scores'][0]} | {target[0]['labels'][0]} -> {correct[0]}")

