import io
import json
import os
import platform
import subprocess as sp
import sys
from pathlib import Path

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image


class ThermalImage:
    """Thermal Image class.
    Partly taken from https://github.com/detecttechnologies/thermal_base and adapted for my needs!
    """

    def __init__(self, image_path, image_name, camera_manufacturer, color_map="jet",
                 process_sensor_data=False, thermal_np=None):
        """Base Class for Thermal Images.
        Args:
            image_path (str): Path of image to be loaded
            image_name (str): Name of the image file
            camera_manufacturer (str, optional): Which type of thermal camera was the image captured from.
                Supported values: ["flir","dji"].
            color_map (str, optional): The default colour map to be used when loading the image. Defaults to "jet".
            thermal_np (np.ndarray): Initialize directly with temp array.
        """

        self.image_path = image_path
        self.image_name = image_name
        # Convert the string false colour map to an opencv object
        self.cmap = getattr(cv, f"COLORMAP_{color_map.upper()}")

        self.max_t = self.max_s = -sys.maxsize - 1
        self.min_t = self.min_s = sys.maxsize
        self.mean_t = None
        self.median_t = None
        self.sd_t = None

        # Load the temperature matrix, sensor matrix and image metadata. First two are loaded as np arrays
        if camera_manufacturer.lower() == "flir":
            self.extract_temperatures_flir()
        elif camera_manufacturer.lower() == "dji":
            self.extract_temperatures_dji(process_sensor_data)
        elif camera_manufacturer.lower() == "pass":
            pass

        if thermal_np is not None:
            self.thermal_np = thermal_np
            self.recalc_stats()

        image = Image.open(os.path.join(self.image_path, self.image_name))
        self.exif = image.info['exif']

    def extract_temperatures_flir(self):
        """Extracts the FLIR-encoded thermal image as 2D floating-point numpy array with temperatures in degC."""
        # read image metadata needed for conversion of the raw sensor values
        # E=1,SD=1,RTemp=20,ATemp=RTemp,IRWTemp=RTemp,IRT=1,RH=50,PR1=21106.77,PB=1501,PF=1,PO=-7340,PR2=0.012545258
        exif_binary = "exiftool.exe" if "win" in sys.platform else "exiftool"
        meta_json = sp.Popen(
            (
                f'{exif_binary} "{os.path.join(self.image_path, self.image_name)}" -Emissivity -ObjectDistance -AtmosphericTemperature '
                "-ReflectedApparentTemperature -IRWindowTemperature -IRWindowTransmission -RelativeHumidity "
                "-PlanckR1 -PlanckB -PlanckF -PlanckO -PlanckR2 -j"
            ),
            shell=True,
            stdout=sp.PIPE,
        ).communicate()[0]

        meta = json.loads(meta_json)[0]

        for key in (
                "ObjectDistance",
                "AtmosphericTemperature",
                "ReflectedApparentTemperature",
                "IRWindowTemperature",
                "RelativeHumidity",
        ):
            meta[key] = parse_from_exif_str(meta[key])

        # exifread can't extract the embedded thermal image, use exiftool instead
        # sp popen can't handle bytes

        thermal_img_bytes = sp.check_output([exif_binary,
                                             "-RawThermalImage",
                                             "-b", f"{os.path.join(self.image_path, self.image_name)}"])

        thermal_img_stream = io.BytesIO(thermal_img_bytes)
        thermal_img = Image.open(thermal_img_stream)
        img_format = thermal_img.format

        # checking for the type of the decoded images
        if img_format == "TIFF":
            raw_sensor_np = np.array(thermal_img)
        # elif img_format == "PNG":
        #     raw_sensor_np = flyr_unpack.unpack(str(self.image_path))

        # raw values -> temperature E=meta['Emissivity']
        thermal_np = sensor_vals_to_temp(raw_sensor_np, **meta)

        return thermal_np, raw_sensor_np, meta

    def extract_temperatures_dji(self, process_sensor_data=False):
        """Extracts the DJI-encoded thermal image as 2D floating-point numpy array with temperatures in degC.
        The raw sensor values are obtained using the sample binaries provided in the official Thermal SDK by DJI.
        The executable file is run and generates a 16 bit unsigned RAW image with Little Endian byte order.
        Link to DJI Forum post: https://forum.dji.com/forum.php?mod=redirect&goto=findpost&ptid=230321&pid=2389016
        """
        # read image metadata for the dji camera images
        exif_binary = "exiftool.exe" if "win" in sys.platform else "exiftool"
        meta_json = sp.Popen(
            (
                f'{exif_binary} "{os.path.join(self.image_path, self.image_name)}" -Emissivity -ObjectDistance -AtmosphericTemperature '
                "-ReflectedApparentTemperature -IRWindowTemperature -IRWindowTransmission -RelativeHumidity "
                "-PlanckR1 -PlanckB -PlanckF -PlanckO -PlanckR2 -Make -Model -AmbientTemperature -j"
            ),
            shell=True,
            stdout=sp.PIPE,
        ).communicate()[0]
        meta = json.loads(meta_json)[0]
        camera_model = meta["Model"]

        e = parse_from_exif_str(meta['Emissivity'])
        meta = {
            "Emissivity": e if e <= 1 else e / 100,
            # "ObjectDistance": parse_from_exif_str(meta['ObjectDistance']),
            "ObjectDistance": 25,
            "AtmosphericTemperature": parse_from_exif_str(meta['AmbientTemperature']),
            "ReflectedApparentTemperature": parse_from_exif_str(meta['AmbientTemperature']),
            "IRWindowTemperature": parse_from_exif_str(meta['AmbientTemperature']),
            "IRWindowTransmission": 1,
            "RelativeHumidity": parse_from_exif_str(meta['RelativeHumidity']),
            "PlanckR1": 21106.77,
            "PlanckB": 1501,
            "PlanckF": 1,
            "PlanckO": -7340,
            "PlanckR2": 0.012545258,
        }

        # calculating the raw sensor values

        # OS and architecture checks
        os_name = "windows" if "win" in sys.platform else "linux"
        architecture_name = "release_x64" if "64bit" in platform.architecture()[0] else "release_x86"
        dji_binary = "dji_irp.exe" if "win" in sys.platform else "dji_irp"
        # dji_executables_url = (
        #     "https://dtpl-ai-public.s3.ap-south-1.amazonaws.com/Thermal_Image_Analysis/DJI_SDK/dji_executables.zip"
        # )
        #
        # # If DJI executable files aren't present, download them and extract the folder contents.
        # if not Path("./external/dji_thermal_sdk").exists():
        #     r = requests.get(dji_executables_url, stream=True)
        #     z = zipfile.ZipFile(io.BytesIO(r.content))
        #     z.extractall()

        if "linux" in os_name:
            # Linux needs path of libdirp.so file added to Environment variable and Execute permission to executable file.
            path_executable = str(Path("./external/dji_thermal_sdk", os_name, architecture_name))
            os.environ["LD_LIBRARY_PATH"] = path_executable
            sp.run(["chmod", "u+x", str(Path(path_executable, dji_binary))])
        elif "windows" in os_name:
            path_executable = str(Path("./external/dji_thermal_sdk", os_name, architecture_name))

        if "MAVIC" not in camera_model:

            if process_sensor_data:
                raw_data_file = "output.raw"
                # Run executable file dji_irp passing image path and prevent output printing to console. Raw file generated.
                sp.run(
                    [str(Path(path_executable, dji_binary)), "-s", f"{os.path.join(self.image_path, self.image_name)}",
                     "-a", "extract", "-o", raw_data_file],
                    universal_newlines=True,
                    stdout=sp.DEVNULL,
                    stderr=sp.STDOUT,
                )
                data_sensor = Path(raw_data_file).read_bytes()
                x_s = np.frombuffer(data_sensor, dtype=np.uint16, count=-1).reshape((512, 640))
                self.max_s = np.amax(x_s)
                self.min_s = np.amin(x_s)
                self.mean_s = np.mean(x_s)
                self.median_s = np.median(x_s)
                self.sd_s = np.std(x_s)
                # Read the contents of the generated output.raw file.
                img = Image.frombytes("I;16L", (640, 512), data_sensor)
                # Extract raw sensor values from generated image into numpy array
                raw_sensor_np = np.array(img)
                os.remove(raw_data_file)
            # ----------
            temp_data_file = "output.temp"
            sp.run(
                [str(Path(path_executable, dji_binary)), "-s", f"{os.path.join(self.image_path, self.image_name)}",
                 "-a", "measure", "--distance", "25", "-o", temp_data_file],
                universal_newlines=True,
                stdout=sp.DEVNULL,
                stderr=sp.STDOUT,
            )
            data_measure = Path(temp_data_file).read_bytes()
            thermal_np = np.frombuffer(data_measure, dtype=np.int16, count=-1).reshape((512, 640))
            self.thermal_np = thermal_np / 10  # temp values in DJI data is multiplied by 10.

            # After the data is read from the output.raw file, remove the file
            os.remove(temp_data_file)
        else:
            # Adding support for MAVIC2-ENTERPRISE-ADVANCED Camera images
            im = Image.open(self.image_path)
            # concatenate APP3 chunks of data
            a = im.applist[3][1]
            for i in range(4, 14):
                a += im.applist[i][1]
            # create image from bytes
            img = Image.frombytes("I;16L", (640, 512), a)

            # extracting the temperatures from thermal images (only works for FLIR sensors)
            # self.thermal_np = sensor_vals_to_temp(raw_sensor_np, **meta)

        self.recalc_stats()

    def generate_colorbar(self, min_temp=None, max_temp=None, cmap=cv.COLORMAP_JET, height=None):
        """Function to generate a colourbar image that can be stitched to the side of your thermal image.
        Args:
            min_temp (int, optional): Minimum temperature on colourbar.
                If not passed, then the image's minimumtemperature is taken. Defaults to None.
            max_temp (int, optional): Maximum temperature on colourbar.
                If not passed, then the image's maximumtemperature is taken. Defaults to None.
            cmap (cv Colormap, optional): Which false colour mapping to use for the colourbar.
                Defaults to cv.COLORMAP_JET.
            height (int, optional): Height of the colourbar canvas to be used.
                If not passed, takes the height of the thermal image. Defaults to None.
        Returns:
            np.ndarray: A colourbar of the required height with temperature values labelled
        """
        min_temp = self.global_min_temp if min_temp is None else min_temp
        max_temp = self.global_max_temp if max_temp is None else max_temp

        # Build a 255 x 21 colour bar, and create an image to append to the main image
        cb_gray = np.arange(255, 0, -1, dtype=np.uint8).reshape((255, 1))
        cb_gray = np.tile(cb_gray, 21)

        # If the image height happens to be smaller than 255 pixels, then reduce the colour bar accordingly
        # if self.thermal_np.shape[0] < 255:
        #     cb_gray = cv.resize(
        #         cb_gray, (min(21, int(self.thermal_np.shape[1] * 0.05)), int(self.thermal_np.shape[0] * 0.75))
        #     )

        cb_color = cv.applyColorMap(cb_gray, cmap) if cmap is not None else cv.cvtColor(cb_gray, cv.COLOR_GRAY2BGR)

        # Decide layout of the colourbar canvas
        cb_canvas_height_slice = height if height is not None else self.thermal_np.shape[0]
        cb_canvas = np.zeros((cb_canvas_height_slice, cb_color.shape[1] + 30, 3), dtype=np.uint8)
        cb_canvas_start_height = cb_canvas.shape[0] // 2 - cb_color.shape[0] // 2
        cb_height_slice = slice(cb_canvas_start_height, cb_canvas_start_height + cb_color.shape[0])

        # Populate the canvas with the colourbar and relevant text
        cb_canvas[cb_height_slice, 10: 10 + cb_color.shape[1]] = cb_color
        min_temp_text = (5, cb_canvas_start_height + cb_color.shape[0] + 30)
        max_temp_text = (5, cb_canvas_start_height - 20)
        cv.putText(cb_canvas, str(round(min_temp, 2)), min_temp_text, cv.FONT_HERSHEY_PLAIN, 1, (255, 0, 0), 1, 8)
        cv.putText(cb_canvas, str(round(max_temp, 2)), max_temp_text, cv.FONT_HERSHEY_PLAIN, 1, (0, 0, 255), 1, 8)

        return cb_canvas

    def save_thermal_image(self, output_path, min_temp=None, max_temp=None):
        """Converts the floating-point temperature array into an image and saves the image.
        Takes the current `self.thermal_np` temperature array, converts it to image-friendly uint8 format. Then it
        computes and stitches a colourbar for required scale. Finally it saves the image to the specified output path
        Args:
            output_path (str / Path): Save destination for the thermal image
        """
        dest = os.path.join(self.image_path, output_path)
        if not os.path.exists(dest):
            os.makedirs(dest)

        # Generate a colour mapped image using default colour map
        cmapped_thermal_img = get_cmapped_temp_image(self.thermal_np, colormap=self.cmap,
                                                     min_temp=min_temp, max_temp=max_temp)
        # cb_canvas = self.generate_colorbar(cmap=self.cmap, min_temp=min_temp, max_temp=max_temp)
        # cmapped_thermal_img = cv.hconcat((cmapped_thermal_img, cb_canvas))
        # cv.imwrite(str(output_path), cmapped_thermal_img)
        cmapped_thermal_img = cv.cvtColor(cmapped_thermal_img, cv.COLOR_BGR2RGB)
        cmapped_thermal_img = Image.fromarray(cmapped_thermal_img)
        p = os.path.join(dest, self.image_name)
        cmapped_thermal_img.save(p, exif=self.exif, quality=100, subsampling=0)
        return cmapped_thermal_img

    def clip_temperatures(self, min_temp, max_temp):
        self.thermal_np = self.thermal_np.clip(min_temp, max_temp)
        self.recalc_stats()

    def standardize_temp(self, new_mean):
        self.thermal_np += (new_mean - self.mean_t)
        self.recalc_stats()

    def standardise_sensor(self, new_mean):
        self.raw_sensor_np = self.raw_sensor_np + int(new_mean - self.mean_s)
        self.min_s = np.amin(self.raw_sensor_np)
        self.max_s = np.amax(self.raw_sensor_np)
        self.mean_s = np.mean(self.raw_sensor_np)
        self.median_s = np.median(self.raw_sensor_np)
        self.sd_s = np.std(self.raw_sensor_np)

    def recalc_stats(self):
        self.min_t = np.amin(self.thermal_np)
        self.max_t = np.amax(self.thermal_np)
        self.mean_t = np.mean(self.thermal_np)
        self.median_t = np.median(self.thermal_np)
        self.sd_t = np.std(self.thermal_np)

    def boost_above_thresh(self, boost_thresh, boost_value):
        self.thermal_np = np.where(self.thermal_np > boost_thresh, self.thermal_np + boost_value, self.thermal_np)
        self.recalc_stats()


def sensor_vals_to_temp(
        raw,
        Emissivity=1.0,
        ObjectDistance=1,
        AtmosphericTemperature=20,
        ReflectedApparentTemperature=20,
        IRWindowTemperature=20,
        IRWindowTransmission=1,
        RelativeHumidity=50,
        PlanckR1=21106.77,
        PlanckB=1501,
        PlanckF=1,
        PlanckO=-7340,
        PlanckR2=0.012545258,
        **kwargs,
):
    """Convert raw values from the thermographic sensor sensor to temperatures in °C. Tested for Flir cams."""
    # this calculation has been ported to python from https://github.com/gtatters/Thermimage/blob/master/R/raw2temp.R
    # a detailed explanation of what is going on here can be found there

    # constants
    ATA1 = 0.006569
    ATA2 = 0.01262
    ATB1 = -0.002276
    ATB2 = -0.00667
    ATX = 1.9

    # transmission through window (calibrated)
    emiss_wind = 1 - IRWindowTransmission
    refl_wind = 0
    # transmission through the air
    h2o = (RelativeHumidity / 100) * np.exp(
        1.5587
        + 0.06939 * (AtmosphericTemperature)
        - 0.00027816 * (AtmosphericTemperature) ** 2
        + 0.00000068455 * (AtmosphericTemperature) ** 3
    )
    tau1 = ATX * np.exp(-np.sqrt(ObjectDistance / 2) * (ATA1 + ATB1 * np.sqrt(h2o))) + (1 - ATX) * np.exp(
        -np.sqrt(ObjectDistance / 2) * (ATA2 + ATB2 * np.sqrt(h2o))
    )
    tau2 = ATX * np.exp(-np.sqrt(ObjectDistance / 2) * (ATA1 + ATB1 * np.sqrt(h2o))) + (1 - ATX) * np.exp(
        -np.sqrt(ObjectDistance / 2) * (ATA2 + ATB2 * np.sqrt(h2o))
    )
    # radiance from the environment
    raw_refl1 = PlanckR1 / (PlanckR2 * (np.exp(PlanckB / (ReflectedApparentTemperature + 273.15)) - PlanckF)) - PlanckO
    raw_refl1_attn = (1 - Emissivity) / Emissivity * raw_refl1  # Reflected component

    raw_atm1 = (
            PlanckR1 / (PlanckR2 * (np.exp(PlanckB / (AtmosphericTemperature + 273.15)) - PlanckF)) - PlanckO
    )  # Emission from atmosphere 1
    raw_atm1_attn = (1 - tau1) / Emissivity / tau1 * raw_atm1  # attenuation for atmospheric 1 emission

    raw_wind = (
            PlanckR1 / (PlanckR2 * (np.exp(PlanckB / (IRWindowTemperature + 273.15)) - PlanckF)) - PlanckO
    )  # Emission from window due to its own temp
    raw_wind_attn = (
            emiss_wind / Emissivity / tau1 / IRWindowTransmission * raw_wind
    )  # Componen due to window emissivity

    raw_refl2 = (
            PlanckR1 / (PlanckR2 * (np.exp(PlanckB / (ReflectedApparentTemperature + 273.15)) - PlanckF)) - PlanckO
    )  # Reflection from window due to external objects
    raw_refl2_attn = (
            refl_wind / Emissivity / tau1 / IRWindowTransmission * raw_refl2
    )  # component due to window reflectivity

    raw_atm2 = (
            PlanckR1 / (PlanckR2 * (np.exp(PlanckB / (AtmosphericTemperature + 273.15)) - PlanckF)) - PlanckO
    )  # Emission from atmosphere 2
    raw_atm2_attn = (
            (1 - tau2) / Emissivity / tau1 / IRWindowTransmission / tau2 * raw_atm2
    )  # attenuation for atmospheric 2 emission

    raw_obj = (
            raw / Emissivity / tau1 / IRWindowTransmission / tau2
            - raw_atm1_attn
            - raw_atm2_attn
            - raw_wind_attn
            - raw_refl1_attn
            - raw_refl2_attn
    )
    val_to_log = PlanckR1 / (PlanckR2 * (raw_obj + PlanckO)) + PlanckF
    if any(val_to_log.ravel() < 0):
        raise Exception("Image seems to be corrupted")
    # temperature from radiance
    return PlanckB / np.log(val_to_log) - 273.15


def parse_from_exif_str(temp_str):
    """String to float parser."""
    # we assume degrees celsius for temperature, metres for length
    if isinstance(temp_str, str):
        return float(temp_str.split()[0])
    return float(temp_str)


def normalize_temp_matrix(thermal_np, min_temp, max_temp):
    """Normalize a temperature matrix to the 0-255 uint8 image range."""
    if min_temp is not None and max_temp is not None:
        min_ = min_temp
        max_ = max_temp
    else:
        min_ = np.amin(thermal_np)
        max_ = np.amax(thermal_np)
    num = thermal_np - min_
    den = max_ - min_
    thermal_np = num / den
    return thermal_np


def get_cmapped_temp_image(thermal_np, colormap=cv.COLORMAP_JET, min_temp=None, max_temp=None):
    """Converts a temperature matrix into a numpy image."""
    thermal_np_norm = normalize_temp_matrix(thermal_np, min_temp, max_temp)
    thermal_image = np.array(thermal_np_norm * 255, dtype=np.uint8)
    if colormap is not None:
        thermal_image = cv.applyColorMap(thermal_image, colormap)
    return thermal_image


def save_histogram(file_name, title, xtitle, scale, values):
    p, f = os.path.split(file_name)
    if not os.path.exists(p):
        os.makedirs(p)
    i = values.astype(int)
    plt.figure()
    plt.hist(x=i, bins=256)
    plt.title(title)
    plt.xlabel(xtitle)
    plt.ylabel("Occurrences")
    plt.yscale(scale)
    #plt.xlim(xlim)
    img_buf = io.BytesIO()
    plt.savefig(img_buf, format='png')
    im = Image.open(img_buf)
    im.save(file_name)
    img_buf.close()
    plt.close()
    return im


def image_histogram_equalization(image, number_bins=256):
    # from http://www.janeriksolem.net/histogram-equalization-with-python-and.html

    # get image histogram
    image_histogram, bins = np.histogram(image.flatten(), number_bins, range=(0, 255), density=True)
    cdf = image_histogram.cumsum() # cumulative distribution function
    cdf = (number_bins-1) * cdf / cdf[-1] # standardize

    # use linear interpolation of cdf to find new pixel values
    image_equalized = np.interp(image.flatten(), bins[:-1], cdf)

    return image_equalized.reshape(image.shape), cdf


if __name__ == '__main__':

    clip_temperature = True
    clip_temp_max = 80
    clip_temp_min = 0

    standardize = False
    standardized_mean = 60

    clip_standardize = False
    clip_std_max = 150
    clip_std_min = 0

    boost_above_thresh = False
    boost_thresh = 70
    boost_value = 10

    equalize = False

    save_histograms = False
    process_sensor_data = False

    image_path = '/home/chris/FH/Master/Datasets/Oekosolar-conv'
    # image_path = '/media/chris/Vol1TB/FH/Datasets/Oekosolar/off-nadir'
    # image_path = 'F:/FH/Datasets/Oekosolar/off-nadir'

    # ---------------------------------------
    print("Reading temperature values from dataset...")

    images = os.listdir(image_path)
    images = [i for i in images if os.path.isfile(os.path.join(image_path, i))]
    images.sort()

    t_images = []
    temp_values = np.array([])
    global_max_t = -sys.maxsize - 1
    global_min_t = sys.maxsize
    global_mean_t = 0
    sum_t = 0
    n_t = 0

    if process_sensor_data:
        global_max_r = -sys.maxsize - 1
        global_min_r = sys.maxsize
        sensor_values = np.array([])

    top_max_t = -sys.maxsize - 1
    top_min_t = sys.maxsize
    top_range_t = -sys.maxsize - 1
    top_max_file = ""
    top_min_file = ""
    top_range_file = ""

    i = 1
    for image in images:

        t_img = ThermalImage(image_path=image_path, image_name=image, camera_manufacturer='dji', color_map='bone',
                             process_sensor_data=process_sensor_data)
        global_min_t, global_max_t = min(global_min_t, t_img.min_t), max(global_max_t, t_img.max_t)
        global_mean_t = (sum_t + t_img.thermal_np.sum()) / (n_t + t_img.thermal_np.size)

        if process_sensor_data:
            global_min_r, global_max_r = min(global_min_r, t_img.min_s), max(global_max_r, t_img.max_s)

        print(f'{i:3}/{len(images):3} {image}: '
              f'Temp min/max : {t_img.min_t:>5.1f} : {t_img.max_t:>5.1f} '
              f' | global min/max/mean : {global_min_t:>5.1f} / {global_max_t:>5.1f} / {global_mean_t:>5.1f}')

        t_images.append(t_img)
        if save_histograms:
            temp_values = np.append(temp_values, t_img.thermal_np)
        if process_sensor_data:
            sensor_values = np.append(sensor_values, t_img.raw_sensor_np)

        if top_max_t < t_img.max_t:
            top_max_t = t_img.max_t
            top_max_file = t_img.image_name
        if top_min_t > t_img.min_t:
            top_min_t = t_img.min_t
            top_min_file = t_img.image_name
        if top_range_t < t_img.max_t - t_img.min_t:
            top_range_t = t_img.max_t - t_img.min_t
            top_range_file = t_img.image_name

        i += 1

    # ---------------------------------------
    print(f'Statistics: Temp mean {global_mean_t:>5.2f}')
    if process_sensor_data:
        global_mean_r = np.mean(sensor_values)
        print(f'Statistics: Sensor mean {global_mean_r:5.0f}')

    print(f"Top min: {top_min_t:>5.1f} : {top_min_file}")
    print(f"Top max: {top_max_t:>5.1f} : {top_max_file}")
    print(f"Top range: {top_range_t:>5.1f} : {top_range_file}")

    # ---------------------------------------
    print(f"Saving temperature data{' and histogram' if save_histograms else ''}...")

    i = 1
    for t_img in t_images:
        print(f"{i:3}/{len(images):3} {t_img.image_name}: Saving temp as image{' and histogram' if save_histograms else ''}")

        if clip_temperature:
            t_img.clip_temperatures(min_temp=clip_temp_min, max_temp=clip_temp_max)
            global_min_t = clip_temp_min
            global_max_t = clip_temp_max

        t_img.save_thermal_image('temperature', min_temp=global_min_t, max_temp=global_max_t)

        if save_histograms:
            h_t = save_histogram(os.path.join(image_path, 'temperature/diag', f'{t_img.image_name}-temp-hist-orig.png'),
                                 f"Temperature Histogram {t_img.image_name}", 'Temperature', 'log',
                                 t_img.thermal_np.flatten(order='C'))
            if process_sensor_data:
                h_r = save_histogram(os.path.join(image_path, 'temperature/diag', f'{t_img.image_name}-sensor-hist-orig.png'),
                                     f"Sensor value Histogram {t_img.image_name}", 'Sensor value', 'log',
                                     t_img.raw_sensor_np.flatten(order='C'))
        i += 1

    if save_histograms:
        save_histogram(os.path.join(image_path, 'temperature/diag', 'total-temp-hist-orig.png'),
                       f"Temperature Histogram (total)", 'Temperature', 'log',
                       temp_values)
        if process_sensor_data:
            save_histogram(os.path.join(image_path, 'temperature/diag', f'total-sensor-hist-orig.png'),
                           f"Sensor value Histogram (total)", 'Sensor value', 'log',
                           sensor_values)

    if standardize:

        # ---------------------------------------
        print("Running standardization of temperature data...")

        temp_values = np.array([])
        global_max_t = -sys.maxsize - 1
        global_min_t = sys.maxsize

        if process_sensor_data:
            global_max_r = -sys.maxsize - 1
            global_min_r = sys.maxsize
            sensor_values = np.array([])

        top_max_t = -sys.maxsize - 1
        top_min_t = sys.maxsize
        top_range_t = -sys.maxsize - 1
        top_max_file = ""
        top_min_file = ""
        top_range_file = ""

        base = 0
        if standardized_mean is None:
            base = global_mean_t
        else:
            base = standardized_mean

        i = 1
        for t_img in t_images:
            print(f'{i:3}/{len(images):3} {t_img.image_name} : Standardize temperature data to {base}. '
                  f'Diff: {base - t_img.mean_t}')

            t_img.standardize_temp(base)

            if clip_standardize:
                t_img.clip_temperatures(min_temp=clip_std_min, max_temp=clip_std_max)
                global_min_t = clip_std_min
                global_max_t = clip_std_max

            if boost_above_thresh:
                t_img.boost_above_thresh(boost_thresh, boost_value)

            global_min_t, global_max_t = min(global_min_t, t_img.min_t), max(global_max_t, t_img.max_t)
            global_mean_t = (sum_t + t_img.thermal_np.sum()) / (n_t + t_img.thermal_np.size)

            if save_histograms:
                temp_values = np.append(temp_values, t_img.thermal_np)

            if top_max_t < t_img.max_t:
                top_max_t = t_img.max_t
                top_max_file = t_img.image_name
            if top_min_t > t_img.min_t:
                top_min_t = t_img.min_t
                top_min_file = t_img.image_name
            if top_range_t < t_img.max_t - t_img.min_t:
                top_range_t = t_img.max_t - t_img.min_t
                top_range_file = t_img.image_name

            i += 1

        # ---------------------------------------
        print(f'Statistics: Temp mean {global_mean_t:>5.2f}')
        print(f"Top min: {top_min_t:>5.1f} : {top_min_file}")
        print(f"Top max: {top_max_t:>5.1f} : {top_max_file}")
        print(f"Top range: {top_range_t:>5.1f} : {top_range_file}")

        # ---------------------------------------
        print(f"Save standardized data{' and histogram' if save_histograms else ''}...")

        i = 1
        for t_img in t_images:
            print(f"{i:3}/{len(images):3} {t_img.image_name} : Save standardized image{' and histogram' if save_histograms else ''}")

            t_img.save_thermal_image('standardized', min_temp=clip_std_min, max_temp=clip_std_max)

            if save_histograms:
                save_histogram(os.path.join(image_path, 'temperature/diag', f'{t_img.image_name}-temp-hist-std.png'),
                               f"Temperature Histogram {t_img.image_name} (std)", 'Temperature', 'log',
                               t_img.thermal_np.flatten(order='C'))
            i += 1

        if save_histograms:
            save_histogram(os.path.join(image_path, 'temperature/diag', 'total-temp-hist-std.png'),
                           f"Temperature Histogram (total, std)", 'Temperature', 'log', temp_values)

    if equalize:

        # ---------------------------------------
        print(f"Perform histogram equalization and save data{' and histogram' if save_histograms else ''}...")

        temp_values = np.array([])
        i = 1
        for t_img in t_images:
            print(f"{i:3}/{len(images):3} {t_img.image_name} : History equalization{' and histogram' if save_histograms else ''}")

            t_img.thermal_np, _ = image_histogram_equalization(t_img.thermal_np)
            t_img.save_thermal_image('equalized', min_temp=0, max_temp=255)

            if save_histograms:
                h_t = save_histogram(os.path.join(image_path, 'temperature/diag', f'{t_img.image_name}-temp-hist-equal.png'),
                                     f"Temperature Histogram {t_img.image_name} (equalized)", 'Temperature', 'log',
                                     t_img.thermal_np.flatten(order='C'))
                temp_values = np.append(temp_values, t_img.thermal_np)
            i += 1

        if save_histograms:
            save_histogram(os.path.join(image_path, 'temperature/diag', 'total-temp-hist-equal.png'),
                           f"Temperature Histogram (total, equal)", 'Temperature', 'log', temp_values)
