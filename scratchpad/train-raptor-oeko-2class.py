import argparse
import sys
import os
import json

import albumentations as A
import albumentations.pytorch.transforms as PA

# add local 'external components' path to sys.path for import
local_libs = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.join(local_libs, "../external"))

from ml.ds import RaptorMaps2ClassDataset
from ml.ds import CocoLikeDataset
from ml.model import ModelTrainer
import ml.augmentation as CT


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Train instance segmentation.')
    parser.add_argument('-c', '--config_file', type=str, required=True,
                        help='Filename of JSON file with params parameters')
    args = parser.parse_args()

    print(f'Training with params from: {args.config_file}')

    # get parameters for training session
    with open(args.config_file, "r") as f:
        config = json.load(f)

    print(f'Config: \n{json.dumps(config, indent=4)}')

    if 'transforms' not in config:
        transforms = A.Compose([
            CT.EmbedToRandomOekosolarImage(image_folder=config['embed_images_folder'],
                                           pattern='DJI_20211018*_T.JPG',
                                           target_to_grayscale=True),
            A.CenterCrop(height=256, width=256),
            A.Resize(height=512, width=512),
            A.Resize(height=800, width=800, p=0.5),
            A.Affine(rotate=(-90, 90), keep_ratio=True, p=0.7),
            A.Affine(translate_percent=(-0.3, 0.3), keep_ratio=True, p=0.8),
            A.Affine(scale=(0.8, 1.2), keep_ratio=True, p=0.3),
            A.Affine(shear=(-10, 10), keep_ratio=True, p=0.3),
            #A.GaussNoise(mean=10, var_limit=(10, 20), p=0.7),
            A.ToFloat(max_value=255),
            PA.ToTensorV2(),
        ], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels']))

        config['transforms'] = A.to_dict(transforms)
    else:
        transforms = A.from_dict(config['transforms'])

    dataset = RaptorMaps2ClassDataset(name=config['experiment'], img_dir=config['images_folder'],
                                      save_images=True, save_masks=True,
                                      transforms=transforms)

    test_transforms = A.Compose([
        A.ToFloat(max_value=255),
        PA.ToTensorV2(),
    ], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels']))

    # load test dataset for model test
    test_dataset = CocoLikeDataset(name=config['experiment'],
                              root=config['test_images_folder'],
                              annFile=config['test_annotation_file'],
                              save_images=config['save_images'],
                              save_masks=config['save_masks'],
                              transforms=test_transforms)

    trainer = ModelTrainer(params=config)

    print(trainer.model)

    print(f'Running training...')
    trainer.train_model(dataset, test_dataset)
