import json
from typing import List
from decimal import Decimal

import numpy as np
import pandas as pd
import matplotlib.pyplot

matplotlib.rcParams['pgf.texsystem'] = 'pdflatex'
matplotlib.pyplot.rc('text', usetex=True)
matplotlib.rcParams.update({'font.family': 'serif', 'font.size': 14})
# matplotlib.rcParams.update({'font.family': 'serif', 'font.size': 18,
#                             'axes.labelsize': 18, 'axes.titlesize': 22,
#                             'figure.titlesize': 10, 'legend.fontsize': 14})
import matplotlib.pyplot as plt
from matplotlib.ticker import IndexLocator, FormatStrFormatter
import os
import argparse
import re

# AP = 'AP @ IoU=0.50:0.95 (all, dets=100)'
# AP50 = 'AP @ IoU=0.50 (all, dets=100)'
# AP75 = 'AP @ IoU=0.75 (all, dets=100)'
# AP_SMALL = 'AP @ IoU=0.50:0.95 (small, dets=100)'
# AP_MEDIUM = 'AP @ IoU=0.50:0.95 (medium, dets=100)'
# AP_LARGE = 'AP @ IoU=0.50:0.95 (large, dets=100)'
# AR1 = 'AR @ IoU=0.50:0.95 (all, dets=1)'
# AR10 = 'AR @ IoU=0.50:0.95 (all, dets=10)'
# AR100 = 'AR @ IoU=0.50:0.95 (all, dets=100)'
# AR_SMALL = 'AR @ IoU=0.50:0.95 (small, dets=100)'
# AR_MEDIUM = 'AR @ IoU=0.50:0.95 (medium, dets=100)'
# AR_LARGE = 'AR @ IoU=0.50:0.95 (large, dets=100)'

# AP = 'AP'
# AP50 = 'AP_{50}'
# AP75 = '$AP_{75}$'
# AP_SMALL = '$AP_{small}$'
# AP_MEDIUM = '$AP_{medium}$'
# AP_LARGE = '$AP_{large}$'
# AR1 = '$AR_{1}$'
# AR10 = '$AR_{10}$'
# AR100 = '$AR_{100}$'
# AR_SMALL = '$AR_{small}$'
# AR_MEDIUM = '$AR_{medium}$'
# AR_LARGE = '$AR_{large}$'


AP = '$AP$'
AP50 = '$AP_{50}$'
AP75 = '$AP_{75}$'
AP_SMALL = '$AP_{small}$'
AP_MEDIUM = '$AP_{medium}$'
AP_LARGE = '$AP_{large}$'
AR1 = '$AR_{1}$'
AR10 = '$AR_{10}$'
AR100 = '$AR_{100}$'
AR_SMALL = '$AR_{small}$'
AR_MEDIUM = '$AR_{medium}$'
AR_LARGE = '$AR_{large}$'


def fexp(number):
    (sign, digits, exponent) = Decimal(number).as_tuple()
    return len(digits) + exponent - 1


def get_param_df(file_name):
    with open(file_name, 'r') as f:
        j = json.load(f)
    s = pd.Series({
        'Epochs': j['epochs'],
        'Batch size': j['batch_size'],
        # 'Optimizer': j['optimizer'],
        'LR': j['lr'],
        # 'Momentum': j['momentum'],
        # 'Weight decay': j['weight_decay'],
        'Step size': j['step_size'],
        'Gamma': j['gamma'],
        # 'Train/Val split': j['train_val_split']
    }, name=j['experiment'])
    return pd.concat([s], axis=1).T


def get_loss_df(file_name):
    loss = np.load(file_name)
    df_losses = pd.DataFrame(loss['losses'], columns=[
        'Epoch', 'Step', 'LR', 'Loss class', 'Loss bbox', 'Loss mask'
    ])
    return df_losses


def get_metrics_df(file_name):
    def numpy_to_df(n):
        return pd.DataFrame(n, columns=[
            'Epoch',
            AP,
            AP50,
            AP75,
            AP_SMALL,
            AP_MEDIUM,
            AP_LARGE,
            AR1,
            AR10,
            AR100,
            AR_SMALL,
            AR_MEDIUM,
            AR_LARGE,
        ])

    metrics = np.load(file_name)
    df_bbox = numpy_to_df(metrics['bbox'])
    df_segm = numpy_to_df(metrics['segm'])

    return df_bbox, df_segm


def get_plot_AP(df, title, subtitle):
    fig, ax = plt.subplots(figsize=(12, 6))

    ax.plot('Epoch', AP, data=df)
    ax.plot('Epoch', AP50, data=df)
    ax.plot('Epoch', AP75, data=df)
    if df[AP_SMALL].min() > -1:
        ax.plot('Epoch', AP_SMALL, data=df)
    if df[AP_MEDIUM].min() > -1:
        ax.plot('Epoch', AP_MEDIUM, data=df)
    if df[AP_LARGE].min() > -1:
        ax.plot('Epoch', AP_LARGE, data=df)

    # Customise some display properties
    plt.ylim([0, 1])
    ax.set_ylabel('AP')
    ax.set_xlabel('Epochs')
    ax.set_title(f'{title} (Average Precision, {subtitle})')
    # ax.set_xticks(df_loss)    # This ensures we have one tick per year, otherwise we get fewer
    # ax.set_xticklabels(years.astype(str).values, rotation='vertical')
    ax.legend(frameon=False)

    return fig


def get_plot_AR(df, title, subtitle):
    fig, ax = plt.subplots(figsize=(12, 6))

    ax.plot('Epoch', AR1, data=df)
    ax.plot('Epoch', AR10, data=df)
    ax.plot('Epoch', AR100, data=df)
    if df[AR_SMALL].min() > -1:
        ax.plot('Epoch', AR_SMALL, data=df)
    if df[AR_MEDIUM].min() > -1:
        ax.plot('Epoch', AR_MEDIUM, data=df)
    if df[AR_LARGE].min() > -1:
        ax.plot('Epoch', AR_LARGE, data=df)

    # Customise some display properties
    plt.ylim([0, 1])
    ax.set_ylabel('AR')
    ax.set_xlabel('Epochs')
    ax.set_title(f'{title} (Average Recall, {subtitle})')
    # ax.set_xticks(df_loss)    # This ensures we have one tick per year, otherwise we get fewer
    # ax.set_xticklabels(years.astype(str).values, rotation='vertical')
    ax.legend()
    return fig


def get_plot_losses(df, subtitle):
    def smooth(scalars: List[float], weight: float) -> List[float]:  # Weight between 0 and 1
        last = scalars[0]  # First value in the plot (first timestep)
        smoothed = list()
        for point in scalars:
            smoothed_val = last * weight + (1 - weight) * point  # Calculate smoothed value
            smoothed.append(smoothed_val)  # Save it
            last = smoothed_val  # Anchor the last smoothed value

        return smoothed

    experiment = re.search(r'.*(exp-plant2-\d+)-.*', subtitle).group(1)
    aug = re.search(r'.*exp-plant2-\d+-\d+-(?:.+?)-(.*)', subtitle)
    if aug is not None:
        aug = aug.group(1)
        experiment = experiment + "-" + aug

    fig, ax1 = plt.subplots(figsize=(12, 6))

    steps = df['Step'].max() + 1
    epochs = df['Epoch'].max() + 1
    x = np.arange(len(df['Epoch']))

    df['Loss class'] = smooth(df['Loss class'], 0.8)
    df['Loss bbox'] = smooth(df['Loss bbox'], 0.8)
    df['Loss mask'] = smooth(df['Loss mask'], 0.8)

    l1 = ax1.plot(x, 'Loss class', data=df)
    l2 = ax1.plot(x, 'Loss bbox', data=df)
    l3 = ax1.plot(x, 'Loss mask', data=df)
    ax1.set_yscale('log')

    min_loss = min(df['Loss class'].min(), df['Loss bbox'].min(), df['Loss mask'].min())
    if min_loss < 1:
        fmt = f'%.{abs(fexp(min_loss))}f'
    else:
        fmt = '%d'
    ax1.yaxis.set_major_formatter(FormatStrFormatter(fmt))

    ax2 = ax1.twinx()
    l4 = ax2.plot(x, 'LR', color="gray", data=df)

    x_ticks = np.arange(0, len(x) + 2, int(steps) * 10)
    x_tick_label = np.arange(0, epochs + 2, 10, dtype=int)

    ax1.xaxis.set_major_locator(IndexLocator(base=10, offset=0))
    plt.xticks(x_ticks, x_tick_label)

    # Customise some display properties
    ax1.set_ylabel('Loss')
    ax2.set_ylabel('LR')
    ax1.set_xlabel('Epochs')
    ax1.set_title(f'Training progress (loss, {experiment})')
    # ax.set_xticks(df_loss)    # This ensures we have one tick per year, otherwise we get fewer
    # ax.set_xticklabels(years.astype(str).values, rotation='vertical')

    lns = l1 + l2 + l3 + l4
    labs = [l.get_label() for l in lns]
    ax1.legend(lns, labs, loc=0, frameon=False)

    return fig


def save_plot_losses(file_name, df, subtitle):
    plt_losses = get_plot_losses(df, subtitle)
    plt.tight_layout()
    plt_losses.savefig(f"{file_name}.eps", format="eps")
    plt.close()


def save_plot_metrics(file_name, df_bbox, df_segm, subtitle):
    title = 'Training evaluation'
    if "test-metrics" in file_name:
        title = 'Test evaluation'

    experiment = re.search(r'.*(exp-plant2-\d+)-.*', subtitle).group(1)
    aug = re.search(r'.*exp-plant2-\d+-\d+-(?:.+?)-(.*)', subtitle)
    if aug is not None:
        aug = aug.group(1)
        experiment = experiment + "-" + aug

    plt_ap_bbox = get_plot_AP(df_bbox, title, f"bbox, {experiment}")
    plt.tight_layout()
    plt_ap_bbox.savefig(f"{file_name}-AP-bbox.eps", format="eps")
    plt.close()

    plt_ar_bbox = get_plot_AR(df_bbox, title, f"bbox, {experiment}")
    plt.tight_layout()
    plt_ar_bbox.savefig(f"{file_name}-AR-bbox.eps", format="eps")
    plt.close()

    plt_ap_segm = get_plot_AP(df_segm, title, f"segm, {experiment}")
    plt.tight_layout()
    plt_ap_segm.savefig(f"{file_name}-AP-segm.eps", format="eps")
    plt.close()

    plt_ar_segm = get_plot_AR(df_segm, title, f"segm, {experiment}")
    plt.tight_layout()
    plt_ar_segm.savefig(f"{file_name}-AR-segm.eps", format="eps")
    plt.close()


def render_metrics_to_latex_table(df, caption=None, label=None):
    # compensate for epochs starting with 0
    df['Epoch'] = df['Epoch'].astype(int) + 1

    a = df['Experiment'].astype(str).str.extract(r'(^exp-plant2-\d+)-.*')[0]
    b = df['Experiment'].astype(str).str.extract(r'^exp-plant2-(?:\d+?)-(?:\d+?)-(?:Adam|SGD)(.*)')[0]
    b = b.str.replace("-", "")
    b = b.replace("", np.nan)
    if len(b.value_counts()) > 0:
        df['Experiment'] = a + "-" + b
    else:
        df['Experiment'] = a

    # set output style for rendering of table
    pd.set_option('display.colheader_justify', 'center')
    pd.set_option('display.precision', 2)

    # remove columns not used for evaluation
    columns = [
        AR1,
        AR10,
        AR100,
        AR_SMALL,
        AR_MEDIUM,
        AR_LARGE,
    ]
    df.drop(columns=columns, axis=1, inplace=True)

    # render dataframe to Latex table
    return df.to_latex(index=False, caption=caption, label=label)


def render_params_to_latex_table(df, caption="", label=""):
    # set output style for rendering of table
    pd.set_option('display.colheader_justify', 'center')
    pd.set_option('display.precision', 2)

    if len(df.index) > 0:
        a = df.index.to_series().astype(str).str.extract(r'(^exp-plant2-\d+)-.*')[0]
        b = df.index.to_series().astype(str).str.extract(r'^exp-plant2-(?:\d+?)-(?:\d+?)-(?:Adam|SGD)(.*)')[0]
        b = b.str.replace("-", "")
        b = b.replace("", np.nan)
        if len(b.value_counts()) > 0:
            df.index = a + "-" + b
        else:
            df.index = a

    # render dataframe to Latex table
    def f1(x):
        return "%.1f" % x

    def f2(x):
        return "%.2f" % x

    def f3(x):
        return "%.3f" % x

    def f4(x):
        return "%.4f" % x

    def f5(x):
        return "%.5f" % x

    def f6(x):
        return "%.6f" % x

    def i(x):
        return "%d" % x

    fun = {
        "f1": f1,
        "f2": f2,
        "f3": f3,
        "f4": f4,
        "f5": f5,
        "f6": f6,
    }

    if df['LR'].min() < 1:
        decim = abs(fexp(df['LR'].min()))
        funLR = fun[f'f{decim}']
    else: funLR = i

    if df['Gamma'].min() < 1:
        decim = abs(fexp(df['Gamma'].min()))
        funGamma = fun[f'f{decim}']
    else:
        funGamma = i

    fmt = {
        "Epochs": i,
        "Batch size": i,
        "LR": funLR,
        "Step size": i,
        "Gamma": funGamma,
    }
    return df.to_latex(index=True, formatters=fmt, label=f"tbl:eval-experiments-params-{label}",
                       caption=f"Experiments and their parameter settings {caption}.")


def process_metrics_data(folder, model_files, filename_ext):
    """Reads metrics files from training sessions and creates plots and Latex table."""

    df_bbox_all = pd.DataFrame()
    df_segm_all = pd.DataFrame()
    for file in model_files:
        print(f"Processing metrics for model {file} {filename_ext}")

        df_bbox, df_segm = get_metrics_df(os.path.join(folder, f'{file}{filename_ext}'))
        experiment = file[0:file[0:file.find(filename_ext)].find("-epoch")]
        save_plot_metrics(os.path.join(folder, f'{file}{filename_ext}'), df_bbox, df_segm, experiment)

        s = pd.concat([pd.Series(experiment, index=['Experiment']), df_bbox.loc[df_bbox['Epoch'].idxmax()]])
        df_bbox_all = pd.concat([df_bbox_all, s.to_frame().T], ignore_index=True)

        s = pd.concat([pd.Series(experiment, index=['Experiment']), df_segm.loc[df_segm['Epoch'].idxmax()]])
        df_segm_all = pd.concat([df_segm_all, s.to_frame().T], ignore_index=True)

    # render dataframe to Latex table
    df_A10 = df_bbox_all.query('Experiment.str.contains("A10")')
    df_A20 = df_bbox_all.query('Experiment.str.contains("A20")')
    df_1 = df_bbox_all.query('Experiment.str.match(".*SGD$")')
    print(
        render_metrics_to_latex_table(df_1, caption="Metrics (bbox)", label="tbl:eval-experiments-metrics-bbox-noaug"))
    print(render_metrics_to_latex_table(df_A10, caption="Metrics (bbox)",
                                        label="tbl:eval-experiments-metrics-bbox-10xaug"))
    print(render_metrics_to_latex_table(df_A20, caption="Metrics (bbox)",
                                        label="tbl:eval-experiments-metrics-bbox-20xaug"))

    df_A10 = df_segm_all.query('Experiment.str.contains("A10")')
    df_A20 = df_segm_all.query('Experiment.str.contains("A20")')
    df_1 = df_segm_all.query('Experiment.str.match(".*SGD$")')
    print(
        render_metrics_to_latex_table(df_1, caption="Metrics (segm)", label="tbl:eval-experiments-metrics-segm-noaug"))
    print(render_metrics_to_latex_table(df_A10, caption="Metrics (segm)",
                                        label="tbl:eval-experiments-metrics-segm-10xaug"))
    print(render_metrics_to_latex_table(df_A20, caption="Metrics (segm)",
                                        label="tbl:eval-experiments-metrics-segm-20xaug"))

    return df_bbox_all, df_segm_all


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Process training statistics.')
    parser.add_argument('--model', type=str, help='Filename of model to use.')
    parser.add_argument('--model_repo', type=str, help='Folder of models to use.')
    parser.add_argument('--model_pattern', type=str, help='Regex pattern to match model names in model_repo.')
    args = parser.parse_args()

    print(f'Model: {args.model}')
    print(f'Model repo: {args.model_repo}')
    print(f'Model pattern: {args.model_pattern}')

    if args.model_repo is not None and args.model_pattern is not None:
        model_repo = args.model_repo
        x = os.listdir(model_repo)
        model_files = [os.path.splitext(name)[0] for name in os.listdir(model_repo)
                       if os.path.isfile(os.path.join(model_repo, name))
                       and re.match(args.model_pattern, name)]
        model_files = list(set(model_files))
        model_files.sort()
    elif args.model is not None:
        model_repo, model_name = os.path.split(args.model)
        model_files = [model_name]
    else:
        raise RuntimeError("Wrong parameters. Either model or model_repo/model_pattern required")

    # ----------------------------------

    df = None
    for file in model_files:
        print(f"Processing params for model {file}")
        if df is None:
            df = get_param_df(os.path.join(model_repo, f'{file}.json'))
        else:
            df = pd.concat([df, get_param_df(os.path.join(model_repo, f'{file}.json'))])

    df_A10 = df.filter(like="A10", axis=0)
    df_A20 = df.filter(like="A20", axis=0)
    df_1 = df.filter(regex="SGD$", axis=0)

    print(render_params_to_latex_table(df_1, caption="No augmentation", label="noaug"))
    print(render_params_to_latex_table(df_A10, caption="10x augmentation", label="10xaug"))
    print(render_params_to_latex_table(df_A20, caption="20x augmentation", label="20xaug"))

    # ----------------------------------

    for file in model_files:
        print(f"Processing loss for model {file}")
        df = get_loss_df(os.path.join(model_repo, f'{file}.losses'))
        experiment = file[0:file[0:file.find(".losses")].find("-epoch")]
        save_plot_losses(os.path.join(model_repo, file), df, experiment)

    # ----------------------------------

    print("Processing metric data")
    process_metrics_data(model_repo, model_files, '.metrics')

    # ----------------------------------

    print("Processing test metric data")
    process_metrics_data(model_repo, model_files, '.test-metrics')
