import argparse
import json
import os
import shutil
import cv2
from ml.ds import CocoLikeDataset
import albumentations as A
import albumentations.pytorch.transforms as PA
import torchvision.transforms as T

# areas/masks with area smaller than threshold will be skipped,
# as they are most likely not masking a complete PV module
AREA_THRESHOLD = 1200

# True if the contours should be visualized.
# Skipped contours will be shown red and need to be confirmed by keypress.
SHOW_CONTOURS = False

def get_contour(mask, bbox, skip_annotation):

    contours, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if SHOW_CONTOURS:
        color = (0, 255, 0)
        wait = 5
        if skip_annotation:
            color = (0, 0, 255)
            wait = 0

        mask = cv2.cvtColor(mask*255, cv2.COLOR_GRAY2RGB)
        img = cv2.drawContours(image=mask, contours=contours, contourIdx=-1, color=color,
                               thickness=1, lineType=cv2.LINE_AA)

        img = cv2.rectangle(img=img, pt1=(bbox[0], bbox[1]), pt2=(bbox[0]+bbox[2], bbox[1]+bbox[3]),
                            color=color, thickness=1)
        cv2.imshow('contours', img)
        cv2.waitKey(wait)

    cont_list = list(contours[0].flatten())
    return [float(c) for c in cont_list]


def get_transforms():
    transforms = A.Compose([
        # A.GaussNoise(mean=10, var_limit=(20, 70), p=0.7),
        # A.ToGray(p=1),
        A.Flip(p=0.5),
        A.Affine(rotate=(-180, 180), keep_ratio=True, p=0.7),
        A.Affine(translate_percent=(-0.2, 0.2), keep_ratio=True, p=0.5),
        A.Affine(scale=(0.9, 1.1), keep_ratio=True, p=0.4),
        A.Affine(shear=(-10, 10), keep_ratio=True, p=0.4),
        # A.GaussNoise(mean=10, var_limit=(10, 20), p=0.7),
        # A.ToFloat(max_value=255),
        PA.ToTensorV2(),
    ], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels']))
    return transforms


def get_annotations(p, target):
    image_id = p * 1000 + target['image_id'].item()

    # sanity check, #boxes, #labels, #areas, #masks need to be identical
    if len(target['boxes']) != len(target['labels']) != len(target['area']) != len(target['masks']):
        print(f'Image {image_id}: #boxes, #labels, #area, #masks not identically, problem with image after transform')
        return None

    annotations = []
    for i in range(len(target['labels'])):

        skip_annotation = False
        area = target['area'][i].item()
        if area < AREA_THRESHOLD:
            print(f'Skip mask {i} of image_id {image_id} due to small area {area}. '
                  f'Not useful!')
            skip_annotation = True

        # create coco style bounding box
        bbox = target['boxes'][i].numpy()
        bbox = [int(c) for c in bbox]
        bbox = [
            bbox[0], bbox[1],
            bbox[2]-bbox[0], bbox[3]-bbox[1],
        ]

        contour = get_contour(target['masks'][i].numpy(), bbox, skip_annotation)

        if not skip_annotation and not len(contour) < 8:
            ann = {
                "id": 0,
                "image_id": image_id,
                "category_id": target['labels'][i].item(),
                "bbox": bbox,
                "segmentation": [
                    contour
                ],
                "area": target['area'][i].item(),
                "iscrowd": target['iscrowd'][i].item(),
                "attributes": {
                    "occluded": False
                }
            }
            annotations.append(ann)
        elif not skip_annotation and len(contour) < 8:
            print(f'Skip mask {i} of image_id {image_id} due to small contour {contour}. '
                  f'Not useful!')

    return annotations


def save_annotations(images, annotations, in_ann, out_ann):

    # create unique id for annotations
    i = 0
    for a in annotations:
        a['id'] = i
        i += 1

    with open(in_ann, 'r') as a:
        i = json.load(a)

    coco_dict = {
        "licenses": i['licenses'],
        "info": i['info'],
        "categories": i['categories'],
        "images": images,
        "annotations": annotations,
    }

    with open(out_ann, 'w') as b:
        json.dump(coco_dict, b, indent=2)


def init_dest_folders(args):
    if os.path.exists(args.out_folder):
        shutil.rmtree(args.out_folder)
    os.makedirs(args.out_folder)
    out_ann_folder, _ = os.path.split(args.out_ann)
    if os.path.exists(out_ann_folder):
        shutil.rmtree(out_ann_folder)
    os.makedirs(out_ann_folder)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Expand dataset by applying transforms to create additional images.')
    parser.add_argument('-e', '--experiment', type=str, required=True,
                        help='Name of the experiment, used for naming the generated files')
    parser.add_argument('-ia', '--in_ann', type=str, required=True,
                        help='COCO annotations file serving for input')
    parser.add_argument('-if', '--in_folder', type=str, required=True,
                        help='Image folder containing images of input dataset')
    parser.add_argument('-oa', '--out_ann', type=str, required=True,
                        help='COCO annotations file for generated output images (w/ path)')
    parser.add_argument('-of', '--out_folder', type=str, required=True,
                        help='Image folder for generated output images')
    parser.add_argument('-p', '--passes', type=int, default=1,
                        help='How often the dataset should be used for generating images')
    args = parser.parse_args()

    transforms = get_transforms()
    config = {
        "experiment": args.experiment,
        "data_repo": args.in_folder,
        "annotation_file": args.in_ann,
    }

    init_dest_folders(args)

    dataset = CocoLikeDataset(name=config['experiment'],
                              root=config['data_repo'], annFile=config['annotation_file'],
                              transforms=transforms)

    images = []
    annotations = []

    for p in range(args.passes):

        for image, target in dataset:

            image_id = target['image_id'].item()

            print(f"Processing image with image_id {image_id}")

            img = T.ToPILImage()(image).convert("RGB")

            ann = get_annotations(p, target)

            if ann is not None and len(ann) > 0:
                file_name = f'{config["experiment"]}-{image_id}-{p}.png'
                img.save(os.path.join(args.out_folder, file_name))
                images.append({
                    "id": p*1000+image_id,
                    "width": img.width,
                    "height": img.height,
                    "file_name": file_name,
                    "license": 0,
                    "flickr_url": "",
                    "coco_url": "",
                    "date_captured": 0
                })
                annotations += ann

    save_annotations(images, annotations, args.in_ann, args.out_ann)
