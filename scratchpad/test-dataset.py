from ml.ds import RaptorMaps2ClassDataset
import albumentations as A
import albumentations.pytorch.transforms as PA
from torch.utils.data.dataloader import DataLoader

transforms = A.Compose([
    PA.ToTensorV2(),
], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['class_labels']))

root = "/home/chris/FH/Master/Datasets/InfraredSolarModules"
dataset = RaptorMaps2ClassDataset('Raptor', root, transforms=transforms)

dataloader = DataLoader(dataset)

for img, target in dataloader:

    print(f'Checking image {target["image_id"][0][0]}: {img.shape[1]}, {img.shape[2]}, {img.shape[3]}')

    channels, height, width = img.shape[1], img.shape[2], img.shape[3]

    for box in target["boxes"]:

        print(f' Box {box[0]}')

        xmin, ymin, xmax, ymax = box[0].tolist()

        assert xmin >= 0
        assert xmax <= width
        assert xmin <= xmax

        assert ymin >= 0
        assert ymax <= height
        assert ymin <= ymax
