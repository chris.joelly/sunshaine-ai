import argparse
import os
import numpy as np
import cv2 as cv

from ml.preprocessing import extract_temp_data, save_thermal_image, get_greyscale_temp_image, \
    standardize_data, center_data, normalize_data, move_to_new_mean, get_dataset_statistics, \
    clip_negative_values, save_histogram


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Standard normalization of image data.')
    parser.add_argument('-c', '--folder', type=str, required=True,
                        help='Name of folder containing images to normalize')
    args = parser.parse_args()

    # if true, all negative temperature values are clipped to 0
    clip_negatives = False

    # different modes for preprocessing the image data
    STANDARDIZE_LOCAL = "local_standardized"
    STANDARDIZE_GLOBAL = "standardized"
    CENTER = "centered"
    NORMALIZE = "normalized"
    FIXED_MEAN = "fixed-mean"
    # processing_modes = [CENTER, STANDARDIZE_GLOBAL, NORMALIZE, FIXED_MEAN]
    processing_modes = [STANDARDIZE_GLOBAL]

    fixed_mean = 40

    TARGET_FOLDER = "preprocessed2"

    dataset_mean = None
    dataset_std = None
    dataset_min = None
    dataset_max = None

    # --------------------------------------------------------------------------------------
    # Read files of dataset

    datasets = [
        # "/media/chris/Vol1TB/FH/Datasets/Oekosolar-orig/20210802/thermo",
        "/media/chris/Vol1TB/FH/Datasets/Oekosolar-orig/20211018/thermo",
        # "/media/chris/Vol1TB/FH/Datasets/Oekosolar-orig/both",
    ]

    for d in datasets:

        print(f"Processing dataset {d}")

        files = os.listdir(d)
        files = [os.path.join(d, f) for f in files if os.path.isfile(os.path.join(d, f))]

        # --------------------------------------------------------------------------------------
        # Extract temperature values from thermal images

        all_temp_values = None

        orig_file_data = {}
        for file in files:
            orig_file_data[file] = extract_temp_data(file)
            min_temp, max_temp, mean_temp, median_temp, std = get_dataset_statistics(orig_file_data[file])
            print(f"Statistics for file {os.path.basename(file)}: min/max/mean/median: "
                  f"{min_temp:>5.1f}/{max_temp:>5.1f}/{mean_temp:>5.1f}/{median_temp:>5.1f}")

            img = get_greyscale_temp_image(orig_file_data[file], min_temp=min_temp, max_temp=max_temp)
            save_thermal_image(img, os.path.join(d, TARGET_FOLDER),
                               f"{file}.png")

            save_histogram(os.path.join(d, TARGET_FOLDER, f"{os.path.basename(file)}-hist.png"),
                           "Temperature histogram", 'Temperature [°C]', 'log',
                           orig_file_data[file].flatten(order='C'), mean_temp, [min_temp, max_temp])
            if all_temp_values is None:
                all_temp_values = orig_file_data[file]
                all_temp_values = all_temp_values.reshape((-1, 512, 640))
            else:
                all_temp_values = np.vstack((all_temp_values, [orig_file_data[file]]))

        min_temp, max_temp, mean_temp, median_temp, std = get_dataset_statistics(all_temp_values)
        print(f"Dataset statistics: min/max/mean/median/std: {min_temp:>5.1f}/{max_temp:>5.1f}/"
              f"{mean_temp:>5.1f}/{median_temp:>5.1f}/{std:>5.1f}")

        save_histogram(os.path.join(d, TARGET_FOLDER, "histogram-orig.png"),
                       "Temperature histogram", 'Temperature [°C]', 'log',
                       all_temp_values.flatten(order='C'), mean_temp, [min_temp, max_temp])

        if clip_negatives:
            # --------------------------------------------------------------------------------------
            # Clip negative temperature values to 0

            all_temp_values = None

            for file in files:

                orig_file_data[file] = clip_negative_values(orig_file_data[file])

                min_temp, max_temp, mean_temp, median_temp, std = get_dataset_statistics(orig_file_data[file])
                print(f"Statistics for file {os.path.basename(file)}: min/max/mean/median: "
                      f"{min_temp:>5.1f}/{max_temp:>5.1f}/{mean_temp:>5.1f}/{median_temp:>5.1f}")
                # save_histogram(os.path.join(d, TARGET_FOLDER, f"{os.path.basename(file)}-hist-neg-clip.png"),
                #                "Temperature histogram (clipped negatives)", 'Temperature [°C]', 'log',
                #                orig_file_data[file].flatten(order='C'), mean_temp, [min_temp, max_temp])
                if all_temp_values is None:
                    all_temp_values = orig_file_data[file]
                    all_temp_values = all_temp_values.reshape((-1, 512, 640))
                else:
                    all_temp_values = np.vstack((all_temp_values, [orig_file_data[file]]))

            min_temp, max_temp, mean_temp, median_temp, std = get_dataset_statistics(all_temp_values)
            print(f"Dataset statistics (neg-clip): min/max/mean/median/std: {min_temp:>5.1f}/{max_temp:>5.1f}/"
                  f"{mean_temp:>5.1f}/{median_temp:>5.1f}/{std:>5.1f}")

            save_histogram(os.path.join(d, TARGET_FOLDER, "histogram-neg-clip.png"),
                           "Temperature histogram (clipped negatives)", 'Temperature [°C]', 'log',
                           all_temp_values.flatten(order='C'), mean_temp, [min_temp, max_temp])

            # use clipped data for further processing
            orig_file_data = orig_file_data.copy()

        neg_clip = '-neg_clip' if clip_negatives else ''

        if dataset_mean is None and dataset_std is None:
            # dataset_mean = mean_temp
            # dataset_std = std
            dataset_mean = 12.427
            dataset_std = 5.270
            dataset_min = min_temp
            dataset_max = max_temp

        # --------------------------------------------------------------------------------------
        # Loop over dataset with varying actions

        subtitle = 'Temperature [°C]'

        for processing_mode in processing_modes:

            sub_folder = os.path.join(TARGET_FOLDER, processing_mode)
            all_temp_values_tmp = all_temp_values.copy()

            # --------------------------------------------------------------------------------------
            # Standardization of temperature data

            if processing_mode == STANDARDIZE_GLOBAL:
                print(f"Standardizing to mean/std {dataset_mean:>5.3f}/{dataset_std:>5.3f}")
                all_temp_values_tmp = standardize_data(all_temp_values_tmp, dataset_mean, dataset_std)
                # all_temp_values_tmp = standardize_data(all_temp_values_tmp)
                # all_temp_values_tmp = center_data(all_temp_values_tmp, 0)
                subtitle = ''
            elif processing_mode == STANDARDIZE_LOCAL:
                all_temp_values_tmp = standardize_data(all_temp_values_tmp)
                subtitle = ''
            elif processing_mode == CENTER:
                all_temp_values_tmp = center_data(all_temp_values_tmp, dataset_mean)
            elif processing_mode == NORMALIZE:
                all_temp_values_tmp = normalize_data(all_temp_values_tmp, dataset_min, dataset_max)
            elif processing_mode == FIXED_MEAN:
                all_temp_values_tmp = move_to_new_mean(all_temp_values_tmp, fixed_mean)

            min_temp, max_temp, mean_temp, median_temp, std = get_dataset_statistics(all_temp_values_tmp)
            print(f"Dataset statistics ({processing_mode}): min/max/mean/median/std: {min_temp:>5.1f}/{max_temp:>5.1f}/"
                  f"{mean_temp:>5.1f}/{median_temp:>5.1f}/{std:>5.1f}")

            save_histogram(os.path.join(d, sub_folder, f"histogram{neg_clip}-{processing_mode}.png"),
                           f"Temperature histogram ({processing_mode})", subtitle, 'log',
                           all_temp_values_tmp.flatten(order='C'), mean_temp, [min_temp, max_temp])

            # --------------------------------------------------------------------------------------
            # Save transformed dataset

            i = 0
            for file in files:
                img = get_greyscale_temp_image(all_temp_values_tmp[i], min_temp=min_temp, max_temp=max_temp)
                save_thermal_image(img, os.path.join(d, sub_folder),
                                   f"{file}"
                                   f"{neg_clip}.png")
                min_temp_f, max_temp_f, mean_temp_f, median_temp_f, std_f = get_dataset_statistics(all_temp_values_tmp[i])
                save_histogram(os.path.join(d, sub_folder, f"{os.path.basename(file)}-{processing_mode}-hist.png"),
                               "Temperature histogram", 'Temperature [°C]', 'log',
                               all_temp_values_tmp[i].flatten(order='C'), mean_temp_f, [min_temp_f, max_temp_f])
                i += 1
