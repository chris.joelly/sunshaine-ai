from ml.ds import OnlineMeanStd, TestDataset


def calc_mean_and_std(dataset):
    mean, std = OnlineMeanStd()(dataset)
    print(f"Data in {dataset.name} has mean = {mean} and std = {std}")
    return mean, std


if __name__ == '__main__':

    calc_mean_and_std(TestDataset(name="Ökosolar", img_dir="F:/FH/PV-Fotos", pattern='DJI_20211018*_T.jpg'))
    calc_mean_and_std(TestDataset(name="RaptorMaps", img_dir="F:/FH/InfraredSolarModules/images"))

# Data in Ökosolar has mean = tensor([0.6205, 0.3095, 0.2973]) and std = tensor([0.3118, 0.2480, 0.2335])
# Data in RaptorMaps has mean = tensor([0.6192, 0.6192, 0.6192]) and std = tensor([0.1549, 0.1549, 0.1549])
