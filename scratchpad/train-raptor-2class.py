import argparse
import sys
import os
import json

import albumentations as A
import albumentations.pytorch.transforms as PA

# add local 'external components' path to sys.path for import
local_libs = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.join(local_libs, "../external"))

from ml.ds import RaptorMaps2ClassDataset
from ml.model import ModelTrainer
import ml.augmentation as CT


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Train instance segmentation.')
    parser.add_argument('-c', '--config_file', type=str, required=True,
                        help='Filename of JSON file with params parameters')
    args = parser.parse_args()

    print(f'Training with params from: {args.config_file}')

    # get parameters for training session
    with open(args.config_file, "r") as f:
        config = json.load(f)

    print(f'Config: \n{json.dumps(config, indent=4)}')

    if 'transforms' not in config:
        transforms = A.Compose([
            CT.EmbedToRandomImage(),
            A.ToGray(p=1),
            A.Affine(rotate=(-90, 90), keep_ratio=True, p=0.7),
            A.Affine(translate_percent=(-0.3, 0.3), keep_ratio=True, p=0.8),
            A.Affine(scale=(0.8, 1.2), keep_ratio=True, p=0.3),
            A.Affine(shear=(-10, 10), keep_ratio=True, p=0.3),
            A.ToFloat(max_value=255),
            PA.ToTensorV2(),
        ], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['class_labels']))

        config['transforms'] = A.to_dict(transforms)
    else:
        transforms = A.from_dict(config['transforms'])

    dataset = RaptorMaps2ClassDataset(name=config['experiment'], img_dir=config['data_repo'],
                                      save_images=False, save_masks=False,
                                      transforms=transforms)

    trainer = ModelTrainer(params=config,
                           image_mean=(0, 0, 0), image_std=(1, 1, 1))

    print(trainer.model)

    for i in range(1, config['passes']+1):
        print(f'Running training pass #{i}')
        trainer.train_model(dataset, config)

