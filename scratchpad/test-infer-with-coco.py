import os.path

import torchvision.transforms as T
import torchvision.transforms.functional as TF

from torchvision.models.detection import mask_rcnn
import torch
from PIL import Image, ImageOps
import myutils
import argparse
import json


def get_cuda_device():
    if torch.cuda.is_available():
        print(f'GPU: {torch.cuda.get_device_name(0)}')
        device = torch.device('cuda')
        torch.cuda.empty_cache()
    else:
        device = torch.device('cpu')
    print(f'Device: {device}')
    return device


def lambda_handler(event, context):
    print("Hello sunshAIne, welcome at Lambda!")
    assert event['file_name'] is not None and len(event['file_name']) > 0, "Missing param 'file_name'!"
    result = os.path.join(os.path.dirname(event['file_name']), 'result.png')
    detect_object(event['file_name'], result)
    return {
        "statusCode:": 200,
        "body": json.dumps({
            "file_name": event['file_name'],
            "result": result,
        })
    }


def detect_object(in_file, out_file):
    model = mask_rcnn.maskrcnn_resnet50_fpn(pretrained=True)
    model.eval()
    #print(model.eval())

    img = Image.open(in_file).convert("RGB")
    img = ImageOps.exif_transpose(img)
    img = T.Resize(size=640)(img)
    img_tensor = TF.to_tensor(img)

    device = get_cuda_device()
    if device.type == 'cuda':
        model.to(device)
        img_tensor = img_tensor.cuda()

    with torch.no_grad():
        predictions = model([img_tensor])

    # print(predictions)

    result, output, top_predictions = myutils.show(img, predictions)
    myutils.save_result(result, out_file)

    return predictions


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Test instance segmentation.')
    parser.add_argument('--input', type=str, required=True, help='Input image for instance segmentation.')
    parser.add_argument('--output', type=str, required=True, help='Name of result image.')
    args = parser.parse_args()

    print(f'Detection on image: {args.input}')
    print(f'Store result to: {args.output}')

    detect_object(args.input, args.output)
