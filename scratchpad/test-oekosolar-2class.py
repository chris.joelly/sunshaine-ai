import argparse
import sys
import os
import fnmatch
from PIL import Image

# add local 'external components' path to sys.path for import
local_libs = os.getcwd()
sys.path.append(os.path.join(local_libs, "external"))

from ml.ds import TestDataset
import albumentations as A
import albumentations.pytorch.transforms as PA
import torchvision.transforms as T

import torch
import myutils
import cv2
import numpy as np

import utils

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Test instance segmentation.')
    parser.add_argument('folder', type=str, help='Folder with JSON annotation file and images in "images" folder.')
    parser.add_argument('--model', type=str, default='model', help='Filename of model to load')
    args = parser.parse_args()

    print(f'Test with data from: {args.folder}')
    print(f'Model: {args.model}')

    model = torch.load(args.model)
    print(model)

    model.eval()

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)

    # --------------------

    transforms = A.Compose([
        A.ToGray(p=1),
        A.ToFloat(max_value=255),
        PA.ToTensorV2(),
    ])

    results_path = os.path.join(args.folder, 'results')
    if not os.path.exists(results_path):
        os.makedirs(results_path)

    pattern = "*"
    image_files = [name for name in os.listdir(args.folder)
                   if os.path.isfile(os.path.join(args.folder, name))
                   and fnmatch.fnmatch(name, pattern)]

    for image_fn in image_files:

        print(f"Processing file {image_fn}...")

        img = Image.open(os.path.join(args.folder, image_fn)).convert('RGB')

        i = transforms(image=np.array(img))['image']

        with torch.no_grad():
            predictions = model([i.cuda()])

        confidentiality = 0.75

        while True:

            output, results, top = myutils.show(img, predictions, confidentiality)

            wait = 1
            if len(predictions) > 0 and len(predictions[0]['labels'].detach().cpu().tolist()) > 0:
                p = predictions[0]
                idx = p['scores'].detach().cpu().numpy() > confidentiality
                if len(idx) > 0:
                    classes = p['labels'].detach().cpu().numpy()[idx]
                    scores = p['scores'].detach().cpu().numpy()[idx]
                    print(f"p>{confidentiality:.2f} -> {image_fn} : {sum(idx)} issues found\n"
                          f"  classes: {classes}\n"
                          f"  scores:  {scores}")
                    wait = 0

                    cv2.imwrite(os.path.join(results_path, image_fn + '-result.jpg') , output)
                    img_corr = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2RGB)
                    side_by_side = np.concatenate((img_corr, output), axis=1)
                    cv2.imwrite(os.path.join(results_path, image_fn + '-sidebyside.jpg'), side_by_side)

                    cv2.imshow('side-by-side', side_by_side)
                    k = cv2.waitKey(wait)
                    if k in [13, 32]:  # RETURN, SPACE
                        break
                    elif k == -1:
                        break
                    elif k == 43:  # +, plus
                        if confidentiality < 1.0:
                            confidentiality += 0.05
                    elif k == 45:  # -, minus
                        if confidentiality > 0.05:
                            confidentiality -= 0.05
                    elif k in (27, 113):  # ESC, q
                        quit(1)
                    else:
                        print(f"{k}: use "
                              f"('RETURN', 'SPACE') for next, "
                              f"('+', '-') for setting confidence val or "
                              f"('q', 'ESC') to quit")
            else:
                print("  nothing found!")
                break
