from osgeo import gdal, osr
tif = gdal.Open('/home/chris/Downloads/Dahlienstrae-18-10-2021-orthophoto-5.tif')
gt = tif.GetGeoTransform()

print(gt)

x_min = gt[0]
x_size = gt[1]
y_min = gt[3]
y_size = gt[5]

mx, my = 500, 600  #coord in map units, as in question
px = mx * x_size + x_min #x pixel
py = my * y_size + y_min #y pixel

print(f'{px} : {py}')

# get the existing coordinate system
old_cs= osr.SpatialReference()
old_cs.ImportFromWkt(tif.GetProjectionRef())

# create the new coordinate system
wgs84_wkt = """
GEOGCS["WGS 84",
    DATUM["WGS_1984",
        SPHEROID["WGS 84",6378137,298.257223563,
            AUTHORITY["EPSG","7030"]],
        AUTHORITY["EPSG","6326"]],
    PRIMEM["Greenwich",0,
        AUTHORITY["EPSG","8901"]],
    UNIT["degree",0.01745329251994328,
        AUTHORITY["EPSG","9122"]],
    AUTHORITY["EPSG","4326"]]"""
new_cs = osr.SpatialReference()
new_cs .ImportFromWkt(wgs84_wkt)

# create a transform object to convert between coordinate systems
transform = osr.CoordinateTransformation(old_cs, new_cs)

#get the point to transform, pixel (0,0) in this case
width = tif.RasterXSize
height = tif.RasterYSize
gt = tif.GetGeoTransform()

x_min = gt[0]
x_size = gt[1]
y_min = gt[3]
y_size = gt[5]

mx, my = 500, 600  #coord in map units, as in question
px = mx * x_size + x_min #x pixel
py = my * y_size + y_min #y pixel

#get the coordinates in lat long
latlong = transform.TransformPoint(px, py)

print(latlong)
