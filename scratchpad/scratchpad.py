
def get_annotation_sizes():
    """calculate min and max sizes of annotated PV modules"""

    import json

    file = "/media/chris/Vol1TB/FH/Datasets/Oekosolar-orig/20211018/thermo/annotatation/oekosolar-fully-annotated/annotations/instances_default.json"
    with open(file, "r") as f:
        meta = json.load(f)

    min_w = 100000
    min_h = 100000
    max_w = 0
    max_h = 0
    for a in meta['annotations']:
        max_w = max(max_w, a['bbox'][2])
        max_h = max(max_h, a['bbox'][3])
        min_w = min(min_w, a['bbox'][2])
        min_h = min(min_h, a['bbox'][3])

    print(f'min w: {min_w} : h: {min_h}')
    print(f'max w: {max_w} : h: {max_h}')

def tile_orthophoto():
    """create tiled orthophoto"""

    from PIL import Image, ImageDraw, ImageOps

    img = Image.open \
        ("/media/chris/3263-3935/DAT/Masterarbeit/Sunshaine-Thesis/images/Dahlienstrae-18-10-2021-orthophoto-standard-normalized.png")

    dw = int(640 - float(img.width - (img.width // 640 ) *640))
    dh = int(512 - float(img.height - (img.height // 512 ) *512))

    img_pad = Image.new(mode="RGBA", size=(img.width +dw, img.height +dh), color=(0, 255, 0))
    img_pad.paste(img)

    draw = ImageDraw.Draw(img_pad)

    line_width = 4

    i = 0
    while i < img_pad.width:
        draw.line((i, 0, i, img_pad.height), fill=(255, 0, 0), width=line_width)
        i += 640

    i = 0
    while i < img_pad.height:
        draw.line((0, i, img_pad.width, i), fill=(255, 0, 0), width=line_width)
        i += 512

    img_pad.save \
        ('/media/chris/3263-3935/DAT/Masterarbeit/Sunshaine-Thesis/images/Dahlienstrae-18-10-2021-orthophoto-standard-normalized-tiled.png')
