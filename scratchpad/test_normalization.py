import time
import glob
from skimage import exposure
import cv2
import numpy as np
import matplotlib.pyplot as plt

# https://towardsdatascience.com/image-enhancement-techniques-using-opencv-and-python-9191d5c30d45
# https://www.researchgate.net/post/How_do_I_normalize_2_grayscale_images_so_that_they_are_equivalent_to_each_other
# https://stackoverflow.com/questions/32655686/histogram-matching-of-two-images-in-python-2-x

imgR = cv2.imread('C:/Users/cjo/Documents/InfraredSolarModules/images/17975.jpg')
imgO = cv2.imread('C:/Users/cjo/Documents/Oekosolar/DJI_20211018163238_0048_T.JPG')
imgR = cv2.cvtColor(imgR, cv2.COLOR_BGR2GRAY)
imgO = cv2.cvtColor(imgO, cv2.COLOR_BGR2GRAY)
cv2.imshow("ImageR", imgR)
cv2.imshow("ImageO", imgO)
cv2.waitKey(0)
cv2.destroyAllWindows()

# -------------------------


def show_histogram(img):
    hist = cv2.calcHist(img, [0], None, [256], [0, 256])
    plt.title("Hist")
    plt.xlabel('bins')
    plt.ylabel("No of pixels")
    plt.plot(hist)
    plt.show()


def show_histogram_pair(img1, img2):
    hist1 = cv2.calcHist(img1, [0], None, [256], [0, 256])
    hist2 = cv2.calcHist(img2, [0], None, [256], [0, 256])
    plt.subplot(121)
    plt.title("Image1-Hist")
    plt.xlabel('bins')
    plt.ylabel("No of pixels")
    plt.plot(hist1)
    plt.subplot(122)
    plt.title("Image2-Hist")
    plt.xlabel('bins')
    plt.ylabel("No of pixels")
    plt.plot(hist2)
    plt.show()


# for i in range(20000):
#     img = cv2.imread(f'C:/Users/cjo/Documents/InfraredSolarModules/images/{i}.jpg')
#     show_histogram(img)
#     time.sleep(0.1)

imgR_eqhist = cv2.equalizeHist(imgR)
imgO_eqhist = cv2.equalizeHist(imgO)
# show_histogram_pair(imgR_eqhist, imgO_eqhist)
#
# show_histogram_pair(imgR, imgR_eqhist)
#
# cv2.imshow("ImagesR", imgR_eqhist)
# cv2.imshow("ImagesO", imgO_eqhist)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# for i in range(20000):
#     img = cv2.imread(f'C:/Users/cjo/Documents/InfraredSolarModules/images/{i}.jpg')
#     img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     img_eqhist = cv2.equalizeHist(img)
#     cv2.imshow("Images1", img)
#     cv2.imshow("Images2", img_eqhist)
#     show_histogram_pair(img, img_eqhist)


files = [f for f in glob.glob("/media/chris/Vol1TB/FH/Datasets/Oekosolar-Gunskirchen-Thermo-Test/DJI_20211018*.JPG")]
# for file in files:
#     img = cv2.imread(file)
#     img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     img_eqhist = cv2.equalizeHist(img)
#     cv2.imshow("Images1", img)
#     cv2.imshow("Images2", img_eqhist)
#     show_histogram_pair(img, img_eqhist)


#clahe = cv2.createCLAHE(clipLimit=40)

# imgR_clahe = clahe.apply(imgR)
# imgO_clahe = clahe.apply(imgO)
# cv2.imshow("Images1", imgR_clahe)
# cv2.imshow("Images2", imgO_clahe)
# show_histogram_pair(imgR_clahe, imgO_clahe)

# for file in files:
#     img = cv2.imread(file)
#     img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     img_clahe = clahe.apply(img)
#
#     cv2.imshow("Images1", img)
#     cv2.imshow("Images2", img_clahe)
#     show_histogram_pair(img, img_clahe)

matched = exposure.match_histograms(imgO, imgR, multichannel=False)
cv2.imshow("Source", imgO)
cv2.imshow("Reference", imgR)
cv2.imshow("Matched", matched)
cv2.waitKey(0)



print('Fin')

