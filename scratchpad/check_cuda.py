import torch

def get_cuda_device():
    if torch.cuda.is_available():
        print(f'GPU: {torch.cuda.get_device_name(0)}')
        device = torch.device('cuda')
        torch.cuda.empty_cache()
    else:
        device = torch.device('cpu')

    print(f'Device: {device}')

    return device


if __name__ == '__main__':
    get_cuda_device()
