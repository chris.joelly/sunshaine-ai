import argparse
import sys
import os

# add local 'external components' path to sys.path for import
local_libs = os.getcwd()
sys.path.append(os.path.join(local_libs, "../external"))

from ml.ds import TestDataset, RaptorMaps2ClassDataset
import ml.augmentation as CT
import albumentations as A
import albumentations.pytorch.transforms as PA
import torchvision.transforms as T

import torch
import myutils
import cv2

import utils


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Test instance segmentation.')
    parser.add_argument('folder', type=str, help='Folder with JSON annotation file and images in "images" folder.')
    parser.add_argument('--model', type=str, required=True, help='Filename of model to load')
    parser.add_argument('--embed_images_folder', required=True, type=str,
                        help='Folder with images where the test images will be embedded to')
    args = parser.parse_args()

    print(f'Test with data from: {args.folder}')
    print(f'Model: {args.model}')
    print(f'Embed images: {args.embed_images_folder}')

    model = torch.load(args.model)
    print(model)

    model.eval()

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)

    # --------------------

    transforms = A.Compose([
        #  A.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
        CT.EmbedToRandomOekosolarImage(image_folder=args.embed_images_folder,
                                       pattern='DJI_20211018*_T.jpg'),
        A.ToGray(p=1),
        A.Affine(rotate=(-90, 90), keep_ratio=True, p=0.7),
        A.Affine(translate_percent=(-0.3, 0.3), keep_ratio=True, p=0.8),
        A.Affine(scale=(0.8, 1.2), keep_ratio=True, p=0.3),
        A.Affine(shear=(-10, 10), keep_ratio=True, p=0.3),
        A.ToFloat(max_value=255),
        PA.ToTensorV2(),
    ], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['class_labels']))
    #dataset = TestDataset('Raptor-Oekosolar-test', args.folder, transforms=transforms)
    dataset = RaptorMaps2ClassDataset('Raptor-Oekosolar-test', args.folder,
                                      save_images=False, save_masks=False,
                                      transforms=transforms)

    data_loader = torch.utils.data.DataLoader(
        dataset, batch_size=1, shuffle=False, num_workers=4,
        collate_fn=utils.collate_fn)

    for image, target in data_loader:

        with torch.no_grad():
            predictions = model([image[0].cuda()])

        i = T.ToPILImage()(image[0]).convert('RGB')
        output, results, top = myutils.show(i, predictions, 0)

        #cv2.imshow('result', output)
        #cv2.waitKey(2)

        if len(predictions) > 0 and len(predictions[0]['labels'].detach().cpu().tolist()) > 0:
            p = predictions[0]
            # print(f"{p['labels']}, {p['scores']}: "
            #       f"{p['labels'][0]}={p['scores'][0]}")

            correct = p['labels'][0].to('cpu') == target[0]['labels'].to('cpu')
            print(f"{target[0]['image_id'][0]}: "
                  f"{p['labels']}, {p['scores']}: "
                  f"{p['labels'][0]}={p['scores'][0]} | {target[0]['labels'][0]} -> {correct[0]}")

            cv2.imwrite(os.path.join(args.folder, f'Raptor-Oekosolar-test-{target[0]["image_id"][0]}-{correct}.png'),
                        output)
