import argparse
import sys
import os
import re

import pandas as pd
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
import matplotlib.pyplot
matplotlib.rcParams['pgf.texsystem'] = 'pdflatex'
matplotlib.pyplot.rc('text', usetex=True)
matplotlib.rcParams.update({'font.family': 'serif', 'font.size': 14})

# 'axes.labelsize': 18, 'axes.titlesize': 18,
#                            'figure.titlesize': 22, 'legend.fontsize': 14

# add local 'external components' path to sys.path for import
local_libs = os.getcwd()
sys.path.append(os.path.join(local_libs, "external"))

from ml.ds import CocoLikeDataset
import albumentations as A
import albumentations.pytorch.transforms as PA
import torchvision.transforms as T

import torch
from torch.utils.data import DataLoader
import numpy as np

import utils


def get_result_plots(df, file_name):

    if df.empty:
        return None

    fig, ax1 = plt.subplots(figsize=(12, 6))

    df.reset_index(drop=True, inplace=True)
    max_F1 = df['F1'].idxmax()

    x = np.arange(len(df['Experiment']))

    ax1.set_title(f'Evaluation detector')
    ax1.set_ylabel('Predictions')
    # ax1.set_yscale("log")
    ax1.set_xlabel('Experiment')
    ax1.set_xticks(x)
    ax1.set_xticklabels(df['Experiment'].astype(str).values, rotation=30, ha='right')

    w = 0.2
    ax1.bar(x-2*w, df['TP'], width=w, color='limegreen', label="TP")
    ax1.bar(x-w, df['FP'], width=w, color='orangered', label="FP")
    ax1.bar(x+w, df['TN'], width=w, color='lightcoral', label="TN")
    ax1.bar(x+2*w, df['FN'], width=w, color='violet', label="FN")

    ax1.axhline(y=df['Annotations'].max(), linestyle="--", color="tab:gray", label='Annotations')

    ax2 = ax1.twinx()
    ax2.set_ylabel('AP / AR / $F_1$-score', labelpad=20)
    ax2.bar(x+w, df['AP'], width=w, color='orchid', label="AP")
    ax2.bar(x+2*w, df['AR'], width=w, color='lightsteelblue', label="AR")
    ax2.bar(x + 3 * w, df['F1'], width=w, color='royalblue', label="F1-score")

    ax1.set_zorder(ax2.get_zorder() + 1)  # put ax1 in front of ax2
    ax1.patch.set_visible(False)  # hide the 'canvas'
    ax2.patch.set_visible(True)  # show the 'canvas'

    xticklabels = ax1.get_xticklabels()
    xticklabels[max_F1].set_text(f'\\textbf{{{xticklabels[max_F1].get_text()}}}')
    ax1.set_xticklabels(xticklabels)

    h1, l1 = ax1.get_legend_handles_labels()
    h2, l2 = ax2.get_legend_handles_labels()

    lgd = ax1.legend(handles=h1+h2, labels=l1+l2,
                     frameon=False, loc="upper left", bbox_to_anchor=(1.10, 1), borderaxespad=0.)

    plt.tight_layout()
    plt.savefig(file_name, bbox_extra_artists=[lgd], dpi=300)

    plt.close()


def get_result_plots_ARAP(df, file_name):

    if df.empty:
        return None

    fig, ax1 = plt.subplots(figsize=(10, 6))

    df = df[df.F1 > 0]

    df.reset_index(drop=True, inplace=True)
    max_F1 = df['F1'].idxmax()

    x = np.arange(len(df['Experiment']))

    ax1.set_title(f'Detector evaluation $F_1$-score', fontsize="24")
    # ax1.set_ylabel('AP / AR')
    # ax1.set_yscale("log")
    plt.ylim([0, 1.05])
    ax1.set_xlabel('Experiment', fontsize="20")
    ax1.set_xticks(x)
    ax1.set_xticklabels(df['Experiment'].astype(str).values, rotation=30, ha='right')

    w = 0.20
    ax1.bar(x-w, df['AP'], width=w, label="AP")
    ax1.bar(x, df['AR'], width=w, label="AR")
    ax1.bar(x+w, df['F1'], width=w, label="F1-score")

    ax1.text(max_F1-w, df.iloc[max_F1].AP + 0.01, f'{df.iloc[max_F1].AP:.2f}', fontsize=11, horizontalalignment='center')
    ax1.text(max_F1, df.iloc[max_F1].AR + 0.01, f'{df.iloc[max_F1].AR:.2f}', fontsize=11, horizontalalignment='center')
    ax1.text(max_F1+w, df.iloc[max_F1].F1 + 0.01, f'{df.iloc[max_F1].F1:.2f}', fontsize=11, horizontalalignment='center')

    xticklabels = ax1.get_xticklabels()
    xticklabels[max_F1].set_text(f'\\textbf{{{xticklabels[max_F1].get_text()}}}')
    ax1.set_xticklabels(xticklabels)

    h1, l1 = ax1.get_legend_handles_labels()

    lgd = ax1.legend(handles=h1, labels=l1, ncol=3,
                     frameon=False, loc="upper right", borderaxespad=0.05)

    plt.tight_layout()
    plt.savefig(file_name, bbox_extra_artists=[lgd], dpi=300)
    plt.close()


def get_result_plots_TPFP(df, file_name):

    if df.empty:
        return None

    fig, ax1 = plt.subplots(figsize=(12, 6))

    df.reset_index(drop=True, inplace=True)
    max_F1 = df['F1'].idxmax()

    x = np.arange(len(df['Experiment']))

    ax1.set_title(f'Detector evaluation Predictions', fontsize="24")
    ax1.set_ylabel('Predictions', fontsize="20")
    # ax1.set_yscale("log")
    ax1.set_xlabel('Experiment', fontsize="20")
    ax1.set_xticks(x)
    ax1.set_xticklabels(df['Experiment'].astype(str).values, rotation=30, ha='right')

    w = 0.2
    ax1.bar(x-2*w, df['TP'], width=w, label="TP")
    ax1.bar(x-w, df['FP'], width=w, label="FP")
    ax1.bar(x+w, df['TN'], width=w, label="TN")
    ax1.bar(x+2*w, df['FN'], width=w, label="FN")

    ax1.axhline(y=df['Annotations'].max(), linestyle="--", color="tab:gray", label='Annotations')

    xticklabels = ax1.get_xticklabels()
    xticklabels[max_F1].set_text(f'\\textbf{{{xticklabels[max_F1].get_text()}}}')
    ax1.set_xticklabels(xticklabels)

    h1, l1 = ax1.get_legend_handles_labels()

    lgd = ax1.legend(handles=h1, labels=l1,
                     frameon=False, loc="upper left", bbox_to_anchor=(1.05, 1), borderaxespad=0.)

    plt.tight_layout()
    plt.savefig(file_name, bbox_extra_artists=[lgd], dpi=300)
    plt.close()


def create_conf_matrix():
    conf_matrix = np.array([
        [true_pos, false_pos],
        [false_neg, true_neg]
    ])
    fig, ax = plt.subplots(figsize=(7.5, 7.5))
    ax.tick_params(axis='both', which='major', labelsize=24)
    axislabels = ['Anomaly', 'No-Anomaly']
    xaxis = np.arange(len(axislabels))
    ax.set_xticks(xaxis)
    ax.set_yticks(xaxis)
    ax.set_xticklabels(axislabels, family='serif')
    ax.set_yticklabels(axislabels, family='serif', rotation=90, verticalalignment="center")
    ax.set_ylabel('Predictions', fontsize=30, family='serif', labelpad=20)
    ax.set_xlabel('Ground truth', fontsize=30, family='serif', labelpad=20)
    ax.set_title(f'Confusion Matrix ({experiment})', fontsize=30, family='serif', pad=50)
    ax.matshow(conf_matrix, cmap=plt.cm.Blues, alpha=0.3)
    for i in range(conf_matrix.shape[0]):
        for j in range(conf_matrix.shape[1]):
            ax.text(x=j, y=i, s=conf_matrix[i, j], va='center', ha='center',
                    fontsize=40, family='serif', color='0')
    plt.tight_layout()
    plt.savefig(os.path.join(result_dir, f"{experiment}-confusion-matrix.svg"), dpi=300, format='svg')

    plt.close()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Test instance segmentation.')
    parser.add_argument('folder', type=str, help='Folder with test images.')
    parser.add_argument('--model', type=str, help='Filename of model to use.')
    parser.add_argument('--model_repo', type=str, help='Folder of models to use.')
    parser.add_argument('--model_pattern', type=str, help='Regex pattern to match model names in model_repo.')
    parser.add_argument('--annotations', type=str, required=True,
                        help='Filename of COCO annotations for evaluating predictions.')
    args = parser.parse_args()

    data_file = os.path.join(args.folder, f"evaluation-test-data.pickle")
    if not (os.path.exists(data_file) and os.path.isfile(data_file)):

        print(f'Test with test images from: {args.folder}')
        print(f'Model: {args.model}')
        print(f'Model repo: {args.model_repo}')
        print(f'Model pattern: {args.model_pattern}')

        if args.model_repo is not None and args.model_pattern is not None:
            model_repo = args.model_repo
            x = os.listdir(model_repo)
            model_files = [os.path.splitext(name)[0] for name in os.listdir(model_repo)
                           if os.path.isfile(os.path.join(model_repo, name))
                           and re.match(args.model_pattern, name)]
            model_files.sort()
        elif args.model is not None:
            model_repo, model_name = os.path.split(args.model)
            model_files = [model_name]
        else:
            raise RuntimeError("Wrong parameters. Either model or model_repo/model_pattern required")

        transforms = A.Compose([
            A.ToFloat(max_value=255),
            PA.ToTensorV2(),
        ], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels']))
        dataset = CocoLikeDataset("CountDetections", root=args.folder, annFile=args.annotations, transforms=transforms)
        dataloader = DataLoader(dataset, batch_size=1, shuffle=False, collate_fn=utils.collate_fn)

        df_prediction_results = pd.DataFrame(columns=[
            'aug', 'no', 'Experiment', 'Annotations', 'Predictions', 'TP', 'FN', 'FP', 'AP', 'AR'
        ])

        for model_file in model_files:

            print(f"Analysing model {model_file}...")

            result_dir = os.path.join(args.folder, model_file)
            os.makedirs(result_dir, exist_ok=True)

            model_file = os.path.join(model_repo, model_file)

            experiment = re.search(r'.*(exp-plant2-\d+)-.*', model_file).group(1)
            experiment_no = re.search(r'.*exp-plant2-(\d+)-.*', model_file).group(1)
            aug = re.search(r'.*exp-plant2-\d+-\d+-(?:Adam|SGD)(-.*-)epoch.*', model_file)
            if aug is not None:
                aug = aug.group(1).replace("-", "")
                experiment = experiment+"-"+aug
            else:
                aug = 'A00'

            model = torch.load(model_file, map_location=torch.device('cpu'))

            device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
            # device = torch.device('cpu')
            model.eval()
            model.to(device)

            true_pos, true_neg, false_pos, false_neg = 0, 0, 0, 0
            num_predictions_total = 0
            num_anomalies = 0
            AP, AR = np.nan, np.nan

            TRANSPARENCY = 0.0
            TINT_COLOR = (0, 0, 0)
            OPACITY = int(255 * TRANSPARENCY)
            SAVE_DETECTIONS = True

            for images, targets in dataloader:

                print(f" processing file {dataset.coco.imgs[int(targets[0]['image_id'])]['file_name']}")
                images = list(image.to(device) for image in images)
                targets = [{k: v.to(device) for k, v in t.items()} for t in targets]
                with torch.no_grad():
                    predictions = model(images)

                p = predictions[0]['scores'] > 0.5
                boxes = predictions[0]['boxes']
                scores = predictions[0]['scores']

                num_predictions_total += len(scores)
                num_anomalies += len(targets[0]['labels'])

                if SAVE_DETECTIONS:
                    transform = T.ToPILImage()
                    img = transform(images[0])
                    img = img.convert('RGBA')
                    overlay = Image.new('RGBA', img.size, TINT_COLOR + (0,))
                    draw = ImageDraw.Draw(overlay)

                def in_targets(targets, center):
                    for box in targets['boxes']:
                        if box[0] <= center[0] <= box[2] and \
                                box[1] <= center[1] <= box[3]:
                            return True
                    return False

                for box, score in zip(boxes, scores):
                    center = (box[0] + (box[2] - box[0]) // 2, box[1] + (box[3] - box[1]) // 2)
                    if score >= 0.5:
                        if in_targets(targets[0], center):
                            true_pos += 1
                            color = (0, 255, 0)         # bright green
                        else:
                            false_pos += 1
                            color = (255, 0, 0)         # bright red
                    else:
                        # check if score below score threshold would be false_neg or true_neg
                        # color detections below threshold differently. dark green/red
                        if in_targets(targets[0], center):
                            false_neg += 1
                            color = (0, 150, 0)
                        else:
                            color = (150, 0, 0)
                            true_neg += 1

                    if SAVE_DETECTIONS:
                        draw.rectangle(box.tolist(), fill=TINT_COLOR + (OPACITY,), width=1, outline=color)
                        img = Image.alpha_composite(img, overlay)

                for gt_box in targets[0]['boxes']:
                    center = (gt_box[0] + (gt_box[2] - gt_box[0]) // 2, gt_box[1] + (gt_box[3] - gt_box[1]) // 2)
                    if not in_targets(predictions[0], center):
                        false_neg += 1
                        if SAVE_DETECTIONS:
                            color = (255, 255, 0)
                            draw.rectangle(gt_box.tolist(), fill=TINT_COLOR + (OPACITY,), width=1, outline=color)
                            img = Image.alpha_composite(img, overlay)

                if SAVE_DETECTIONS:
                    img.save(os.path.join(result_dir,
                                          f"{dataset.coco.imgs[int(targets[0]['image_id'])]['file_name']}-detections.png"))

            print(f"Model: {model_file}")
            print(f"a / p: {num_anomalies} / {num_predictions_total}")
            print(f"TP = {true_pos}, FP = {false_pos}")
            print(f"FN = {false_neg}, TN = {true_neg}")
            if (true_pos > 0) and (true_pos+false_pos) > 0 and (true_pos+false_neg) > 0:
                AP = true_pos/(true_pos+false_pos)
                AR = true_pos/(true_pos+false_neg)
                F1 = 2*(AP*AR)/(AP+AR)
            else:
                AP, AR, F1 = 0, 0, 0

            print(f"Precision/Recall/F1-score = {AP}/{AR}/{F1}")

            data = {
                'aug': aug,
                'no': experiment_no,
                'Experiment': experiment,
                'Annotations': num_anomalies,
                'Predictions': num_predictions_total,
                'TP': true_pos, 'FN': false_neg, 'FP': false_pos, 'TN': true_neg,
                'AP': AP, 'AR': AR, 'F1': F1
            }
            s = pd.Series(data)
            df_prediction_results = pd.concat([df_prediction_results, s.to_frame().T], ignore_index=True)

            create_conf_matrix()

        df = df_prediction_results

        df.to_pickle(path=data_file)
    else:
        df = pd.read_pickle(data_file)

    df.set_index(keys=['aug', 'no'], drop=True, inplace=True)
    idx = df.index
    df.index = df.index.set_levels(idx.unique(level=1).astype(int), level=1)
    df.sort_index(level=["aug", "no"], sort_remaining=False, inplace=True)

    df['AP'] = pd.to_numeric(df['AP'], errors='coerce')
    df['AR'] = pd.to_numeric(df['AR'], errors='coerce')
    df['F1'] = pd.to_numeric(df['F1'], errors='coerce')
    df['TP'] = pd.to_numeric(df['TP'], errors='coerce')
    df['FP'] = pd.to_numeric(df['FP'], errors='coerce')
    df['TN'] = pd.to_numeric(df['TN'], errors='coerce')
    df['FN'] = pd.to_numeric(df['FN'], errors='coerce')

    df_A10 = df[df['Experiment'].str.contains('A10')]
    df_A20 = df[df['Experiment'].str.contains('A20')]

    df_O = df.drop(df.loc[df['Experiment'].str.contains('A10')].index)
    df_O = df_O.drop(df_O.loc[df['Experiment'].str.contains('A20')].index)

    get_result_plots(df_O, os.path.join(args.folder, f"evaluation-test.eps"))
    get_result_plots(df_A10, os.path.join(args.folder, f"evaluation-test-A10.eps"))
    get_result_plots(df_A20, os.path.join(args.folder, f"evaluation-test-A20.eps"))

    get_result_plots_ARAP(df_O, os.path.join(args.folder, f"evaluation-test-AP.eps"))
    get_result_plots_ARAP(df_A10, os.path.join(args.folder, f"evaluation-test-AP-A10.eps"))
    get_result_plots_ARAP(df_A20, os.path.join(args.folder, f"evaluation-test-AP-A20.eps"))

    get_result_plots_TPFP(df_O, os.path.join(args.folder, f"evaluation-test-TP.eps"))
    get_result_plots_TPFP(df_A10, os.path.join(args.folder, f"evaluation-test-TP-A10.eps"))
    get_result_plots_TPFP(df_A20, os.path.join(args.folder, f"evaluation-test-TP-A20.eps"))