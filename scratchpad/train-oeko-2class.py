import argparse
import sys
import os
import json

import albumentations as A
import albumentations.pytorch.transforms as PA

# add local 'external components' path to sys.path for import
local_libs = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.join(local_libs, "external"))

from ml.ds import CocoLikeDataset
from ml.model import ModelTrainer


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Train instance segmentation.')
    parser.add_argument('-c', '--config_file', type=str, required=True,
                        help='Filename of JSON file with params parameters')
    args = parser.parse_args()

    print(f'Training with params from: {args.config_file}')

    # get parameters for training session
    with open(args.config_file, "r") as f:
        config = json.load(f)

    print(f'Config: \n{json.dumps(config, indent=4)}')

    if 'transforms' not in config:
        transforms = A.Compose([
            A.ToFloat(max_value=255),
            PA.ToTensorV2(),
        ], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels']))
        config['transforms'] = A.to_dict(transforms)
    else:
        transforms = A.from_dict(config['transforms'])

    dataset = CocoLikeDataset(name=config['experiment'],
                              root=config['image_folder'], annFile=config['annotation_file'],
                              save_images=config['save_images'], save_masks=config['save_masks'],
                              transforms=transforms)

    trainer = ModelTrainer(params=config)

    print(trainer.model)

    trainer.train_model(dataset)
