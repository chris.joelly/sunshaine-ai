import myutils

def test_detection():
    model = myutils.get_model(myutils.get_device())
    img_path = '../images/7.jpg'
    img = myutils.get_image(img_path)
    predictions = myutils.detect_in_image(img, model)
    print(predictions)
    result, output, top_predictions = myutils.show(img, predictions)
    myutils.save_result(result, f'{img_path}.result.jpg')

test_detection()