import matplotlib.pyplot as plt
from skimage import io
import pandas as pd


def process(filename):
    img = io.imread(filename, plugin='pil')

    io.imshow(img)
    plt.show()
    print("")

    df = pd.DataFrame(img.flatten())
    print(df.describe())


if __name__ == '__main__':
    process('/media/chris/Vol1TB/FH/Datasets/Oekosolar-Gunskirchen-Thermo-Test/ir_calib/DJI_20211018161525_0021_T.tif')
    process('/media/chris/Vol1TB/FH/Datasets/Oekosolar-Gunskirchen-Thermo-Test/ir_calib/DJI_20211018161528_0022_T.tif')
