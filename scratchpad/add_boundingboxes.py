import argparse
import fnmatch
import sys
import os
from PIL import Image, ImageDraw

# add local 'external components' path to sys.path for import
local_libs = os.getcwd()
sys.path.append(os.path.join(local_libs, "external"))

from ml.ds import CocoLikeDataset
import albumentations as A
import albumentations.pytorch.transforms as PA
import torchvision.transforms as T

from torch.utils.data import DataLoader

import utils

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='draw annotations to dataset images.')
    parser.add_argument('folder', type=str, help='Folder with dataset images.')
    parser.add_argument('--annotations', type=str, required=True,
                        help='Filename of COCO annotations for bbox data.')
    args = parser.parse_args()

    print(f'Test with test images from: {args.folder}')
    print(f'Use annotations from: {args.annotations}')

    result_dir = os.path.join(args.folder, "annotated")
    os.makedirs(result_dir, exist_ok=True)

    transforms = A.Compose([
        A.ToFloat(max_value=255),
        PA.ToTensorV2(),
    ], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels']))
    dataset = CocoLikeDataset("Blah", root=args.folder, annFile=args.annotations, transforms=transforms)
    dataloader = DataLoader(dataset, batch_size=1, shuffle=False, collate_fn=utils.collate_fn)

    TRANSPARENCY = 0.0
    TINT_COLOR = (0, 0, 0)
    OPACITY = int(255 * TRANSPARENCY)

    for images, targets in dataloader:

        print(f"Processing image {dataset.coco.imgs[int(targets[0]['image_id'])]['file_name']}")

        transform = T.ToPILImage()
        img = transform(images[0])
        img = img.convert('RGBA')
        overlay = Image.new('RGBA', img.size, TINT_COLOR + (0,))
        draw = ImageDraw.Draw(overlay)

        for box in targets[0]['boxes']:
            center = (box[0] + (box[2] - box[0]) // 2, box[1] + (box[3] - box[1]) // 2)
            color = (0, 255, 0)
            draw.rectangle(box.tolist(), fill=TINT_COLOR + (OPACITY,), width=1, outline=color)
            img = Image.alpha_composite(img, overlay)

            img.save(os.path.join(result_dir,
                                  f"{dataset.coco.imgs[int(targets[0]['image_id'])]['file_name']}-annotated.png"))

