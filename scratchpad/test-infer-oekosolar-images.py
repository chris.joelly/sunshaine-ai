import argparse

import sys
import os

# add local 'external components' path to sys.path for import
local_libs = os.getcwd()
sys.path.append(os.path.join(local_libs, "external"))

import PIL.Image
from ml.model import CustomMaskRCNNModelManager
from ml.ds import TestDataset
import torch
from torch.utils.data import DataLoader
import myutils
import cv2
import numpy as np

import torchvision.transforms as T
import albumentations as A


import utils


def tensor_to_image(tensor):
    tensor = tensor*255
    tensor = np.array(tensor, dtype=np.uint8)
    if np.ndim(tensor)>3:
        assert tensor.shape[0] == 1
        tensor = tensor[0]
    return PIL.Image.fromarray(tensor)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Test instance segmentation.')
    parser.add_argument('folder', type=str, help='Folder with image dataset')
    parser.add_argument('--model', type=str, default='maskrcnn-raptor-instance.1', help='Filename of model')
    args = parser.parse_args()

    print(f'Test with images from: {args.folder}')
    print(f'Model: {args.model}')

    num_classes = 3  # test simple case: background, No-Anomaly, Anomaly

    modelMgr = CustomMaskRCNNModelManager(3)
    model = modelMgr.load_model(args.model)
    model.eval()

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.to(device)

    # --------------------

    transforms = A.Compose([
        #  T.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
        A.ToGray(p=1),
    ])

    dataset = TestDataset('Raptor-Oekosolar-Test', args.folder, transforms=transforms, pattern='DJI_20211018*_T.jpg')
    data_loader = DataLoader(
        dataset, batch_size=1, shuffle=False, num_workers=4,
        collate_fn=utils.collate_fn)

    for image in data_loader:
        i = T.ToPILImage()(image[0][0]).convert('RGB')

        i = np.array(i)
        i = np.transpose(i, (2, 0, 1))  # convert from HxWxC format to CxHxW format
        i = torch.as_tensor(i/255, dtype=torch.float32)

        with torch.no_grad():
            predictions = model([i.cuda()])

        i = T.ToPILImage()(image[0][0]).convert('RGB')
        output, results, top = myutils.show(i, predictions, 0)

        cv2.imshow('result', output)
        cv2.waitKey(2)

        if len(predictions) > 0 and len(predictions[0]['labels'].detach().cpu().tolist()):
            p = predictions[0]
            print(f"{p['labels']}, {p['scores']}: "
                  f"{p['labels'][0]}={p['scores'][0]}")

